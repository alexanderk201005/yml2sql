﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YmlModel;
using YmlModel.Model;

namespace YmlDb
{
    public class YmlDbContext : DbContext
    {
        public virtual DbSet<YmlAge> Ages { get; set; }
        public virtual DbSet<YmlCategory> Categories { get; set; }
        public virtual DbSet<YmlCategoryId> CategoryIds { get; set; }
        public virtual DbSet<YmlCurrency> Currencies { get; set; }
        public virtual DbSet<YmlShopDeliveryOption> ShopDeliveryOptions { get; set; }
        public virtual DbSet<YmlDiscount> Discounts { get; set; }
        public virtual DbSet<YmlDiscountPrice> DiscountPrices { get; set; }
        public virtual DbSet<YmlGift> Gifts { get; set; }
        public virtual DbSet<YmlOffer> Offers { get; set; }
        public virtual DbSet<YmlOfferDeliveryOption> OfferDeliveryOptions { get; set; }
        public virtual DbSet<YmlParam> Params { get; set; }
        public virtual DbSet<YmlPrice> Prices { get; set; }
        public virtual DbSet<YmlProduct> Products { get; set; }
        public virtual DbSet<YmlPriceLabsParam> PriceLabsParams { get; set; }
        public virtual DbSet<YmlPromo> Promos { get; set; }
        public virtual DbSet<YmlPromoGift> PromoGifts { get; set; }
        public virtual DbSet<YmlPurchase> Purchases { get; set; }
        public virtual DbSet<YmlShop> Shops { get; set; }
        public virtual DbSet<YmlCatalog> Catalogs { get; set; }

        public YmlDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
            Configuration.ProxyCreationEnabled = false;
            //Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("public");

            base.OnModelCreating(modelBuilder);
        }
    }
}
