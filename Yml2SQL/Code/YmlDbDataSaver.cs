﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Yml2SQL.Interfaces;
using YmlDb;
using YmlModel.Model;

namespace Yml2SQL.Code
{
    public class YmlDbDataSaver : IDataSaver
    {
        private readonly ILog _log;

        public YmlDbDataSaver(ILog log)
        {
            _log = log;

            Database.SetInitializer(new YmlDbInitializer());
        }

        public void SaveCatalog(YmlCatalog catalog)
        {
            _log.Info("Сохранение каталога в базу...");

            using (var unitOfWork = new YmlUnitOfWork())
            {
                try
                {
                    var catalogRepository = unitOfWork.GetRepository<YmlCatalog>();
                    var shopsRepository = unitOfWork.GetRepository<YmlShop>();

                    _log.InfoFormat("Проверка наличия каталога с датой {0}.", catalog.Date);

                    var existingCatalog = catalogRepository.Entities
                        .SingleOrDefault(r => r.Date == catalog.Date);

                    if (existingCatalog == null)
                    {
                        _log.InfoFormat("Каталог с датой {0} не найден.", catalog.Date);
                        _log.InfoFormat("Добавление нового каталога.");

                        catalogRepository.Add(catalog);
                    }
                    else
                    {
                        _log.Info("Загрузка данных из каталога...");

                        var shop = shopsRepository.Entities
                            .Include(c => c.Categories)
                            .Include(c => c.Currencies)
                            .Include(c => c.DeliveryOptions)
                            .Include(c => c.Gifts)
                            .Include(c => c.Offers.Select(r => r.Barcodes))
                            .Include(c => c.Offers.Select(r => r.DeliveryOptions))
                            .Include(c => c.Offers.Select(r => r.DataTours))
                            .Include(c => c.Offers.Select(r => r.Params))
                            .Include(c => c.Offers.Select(r => r.Pictures))
                            .Include(c => c.Offers.Select(r => r.PricelabsParams))
                            .Include(c => c.Offers.Select(r => r.Age))
                            .Include(c => c.Offers.Select(r => r.CategoryId))
                            .Include(c => c.Offers.Select(r => r.Price))
                            .Include(c => c.Promos.Select(r => r.Discount))
                            .Include(c => c.Promos.Select(r => r.PromoGifts))
                            .Include(c => c.Promos.Select(r => r.Purchase.Products))
                            .Include("Promos.Purchase.Products.DiscountPrice")
                            .Include(c => c.Promos.Select(r => r.Purchase.Products))
                            .SingleOrDefault(r => r.Key == existingCatalog.Key);


                        if (shop == null)
                        {
                            _log.ErrorFormat("Каталог с датой {0} не найден.", catalog.Date);
                            return;
                        }

                        _log.InfoFormat("Обновление данных...");

                        UpdateShop(unitOfWork, shop, catalog.Shop);

                        shopsRepository.Update(shop);
                    }

                    _log.Info("Сохранение каталога в базу...");

                    unitOfWork.Commit();

                    _log.Info("Сохранение каталога в базу завершено.");
                }
                catch (Exception ex)
                {
                    _log.ErrorFormat("Ошибка сохранения каталога в базу: {0}", ex.Message);
                }
            }
        }

        private void UpdateShop(IUnitOfWork unitOfWork, YmlShop shop, YmlShop newShop)
        {
            _log.InfoFormat("Обновление магазина...");

            newShop.Key = shop.Key;

            unitOfWork.DbContext.Entry(shop)
                .CurrentValues.SetValues(newShop);

            shop.Agency = newShop.Agency;
            shop.Company = newShop.Company;
            shop.Email = newShop.Email;
            shop.Name = newShop.Name;
            shop.Platform = newShop.Platform;
            shop.Url = newShop.Url;
            shop.Version = newShop.Version;

            unitOfWork.ObjectStateManager.GetObjectStateEntry(shop)
                .ChangeState(EntityState.Modified);

            _log.InfoFormat("Магазин: {0}. Компания: {1}", newShop.Name, newShop.Company);
            _log.InfoFormat("Url магазина: {0}.", newShop.Url);

            UpdateShopCategories(unitOfWork, shop, newShop);
            UpdateShopCurrencies(unitOfWork, shop, newShop);
            UpdateShopDeliveryOptions(unitOfWork, shop, newShop);
            UpdateShopGifts(unitOfWork, shop, newShop);
            UpdateShopOffers(unitOfWork, shop, newShop);
            UpdateShopPromos(unitOfWork, shop, newShop);
        }

        private void UpdateShopCategories(IUnitOfWork unitOfWork, YmlShop shop, YmlShop newShop)
        {
            _log.InfoFormat("Обновление категорий...");

            foreach (var existingChild in shop.Categories.ToArray())
            {
                if (!newShop.Categories.Any(u => u.Id.Equals(existingChild.Id)))
                {
                    _log.InfoFormat("Удаление категории: {0}", existingChild.Value);

                    unitOfWork.ObjectContext.DeleteObject(existingChild);
                }
            }

            foreach (var childModel in newShop.Categories)
            {
                var existingChild = shop.Categories
                    .SingleOrDefault(c => c.Id == childModel.Id);

                if (existingChild != null)
                {
                    _log.InfoFormat("Обновление категории: {0}", childModel.Value);

                    existingChild.Value = childModel.Value;
                    existingChild.ParentId = childModel.ParentId;

                    unitOfWork.ObjectStateManager.GetObjectStateEntry(existingChild)
                        .ChangeState(EntityState.Modified);
                }
                else
                {
                    _log.InfoFormat("Добавление категории: {0}", childModel.Value);
                    shop.Categories.Add(childModel);
                }
            }
        }

        private void UpdateShopCurrencies(IUnitOfWork unitOfWork, YmlShop shop, YmlShop newShop)
        {
            _log.InfoFormat("Обновление валют...");

            foreach (var existingChild in shop.Currencies.ToArray())
            {
                if (!newShop.Currencies.Any(u => u.Id.Equals(existingChild.Id)))
                {
                    _log.InfoFormat("Удаление валюты: {0}", existingChild.Id);

                    unitOfWork.ObjectContext.DeleteObject(existingChild);
                }
            }

            foreach (var childModel in newShop.Currencies)
            {
                var existingChild = shop.Currencies
                    .SingleOrDefault(c => c.Id == childModel.Id);

                if (existingChild != null)
                {
                    _log.InfoFormat("Обновление валюты: {0}", childModel.Id);

                    existingChild.Id = childModel.Id;
                    existingChild.Plus = childModel.Plus;
                    existingChild.Rate = childModel.Rate;

                    unitOfWork.ObjectStateManager.GetObjectStateEntry(existingChild)
                        .ChangeState(EntityState.Modified);
                }
                else
                {
                    _log.InfoFormat("Добавление валюты: {0}", childModel.Id);
                    shop.Currencies.Add(childModel);
                }
            }
        }

        private void UpdateShopDeliveryOptions(IUnitOfWork unitOfWork, YmlShop shop, YmlShop newShop)
        {
            _log.InfoFormat("Обновление информации о доставке...");

            foreach (var existingChild in shop.DeliveryOptions.ToArray())
            {
                if (!newShop.DeliveryOptions.Any(
                    u => u.Cost.Equals(existingChild.Cost)
                         && u.Days.Equals(existingChild.Days)
                         && u.OrderBefore.Equals(existingChild.OrderBefore)))
                {
                    _log.InfoFormat("Удаление информации о доставке: {0}", existingChild.OrderBefore);

                    unitOfWork.ObjectContext.DeleteObject(existingChild);
                }
            }

            foreach (var childModel in newShop.DeliveryOptions)
            {
                var existingChild = shop.DeliveryOptions
                    .SingleOrDefault(c => c.Cost.Equals(childModel.Cost)
                                          && c.Days.Equals(childModel.Days)
                                          && c.OrderBefore.Equals(childModel.OrderBefore));

                if (existingChild != null)
                {
                    _log.InfoFormat("Обновление информации о доставке: {0}", childModel.OrderBefore);

                    existingChild.Cost = childModel.Cost;
                    existingChild.Days = childModel.Days;
                    existingChild.OrderBefore = childModel.OrderBefore;

                    unitOfWork.ObjectStateManager.GetObjectStateEntry(existingChild)
                        .ChangeState(EntityState.Modified);
                }
                else
                {
                    _log.InfoFormat("Добавление информации о доставке: {0}", childModel.OrderBefore);
                    shop.DeliveryOptions.Add(childModel);
                }
            }
        }

        private void UpdateShopGifts(IUnitOfWork unitOfWork, YmlShop shop, YmlShop newShop)
        {
            _log.InfoFormat("Обновление информации о подарках...");

            foreach (var existingChild in shop.Gifts.ToArray())
            {
                if (!newShop.Categories.Any(u => u.Id.Equals(existingChild.Id)))
                {
                    _log.InfoFormat("Удаление информации о подарке: {0}", existingChild.Name);

                    unitOfWork.ObjectContext.DeleteObject(existingChild);
                }
            }

            foreach (var childModel in newShop.Gifts)
            {
                var existingChild = shop.Gifts
                    .SingleOrDefault(c => c.Id == childModel.Id);

                if (existingChild != null)
                {
                    _log.InfoFormat("Обновление информации о подарке: {0}", childModel.Name);

                    existingChild.Id = childModel.Id;
                    existingChild.Name = childModel.Name;
                    existingChild.Picture = childModel.Picture;

                    unitOfWork.ObjectStateManager.GetObjectStateEntry(existingChild)
                        .ChangeState(EntityState.Modified);
                }
                else
                {
                    _log.InfoFormat("Добавление информации о подарке: {0}", childModel.Name);

                    shop.Gifts.Add(childModel);
                }
            }
        }

        private void UpdateShopOffers(IUnitOfWork unitOfWork, YmlShop shop, YmlShop newShop)
        {
            _log.InfoFormat("Обновление предложений магазина...");

            foreach (var existingChild in shop.Offers.ToArray())
            {
                if (!newShop.Offers.Any(
                    u => u.Id.Equals(existingChild.Id)))
                {
                    _log.InfoFormat("Удаление предложения магазина: {0}", existingChild.Id);

                    unitOfWork.ObjectContext.DeleteObject(existingChild);
                }
            }

            foreach (var childModel in newShop.Offers)
            {
                var existingChild = shop.Offers
                    .SingleOrDefault(c => c.Id.Equals(childModel.Id));

                if (existingChild != null)
                {
                    _log.InfoFormat("Обновление предложения магазина: {0}", childModel.Id);

                    existingChild.ISBN = childModel.ISBN;
                    existingChild.Adult = childModel.Adult;
                    existingChild.Artist = childModel.Artist;
                    existingChild.Author = childModel.Author;
                    existingChild.Binding = childModel.Binding;
                    existingChild.Country = childModel.Country;
                    existingChild.CountryOfOrigin = childModel.CountryOfOrigin;
                    existingChild.CurrencyId = childModel.CurrencyId;
                    existingChild.Date = childModel.Date;
                    existingChild.Days = childModel.Days;
                    existingChild.Delivery = childModel.Delivery;
                    existingChild.Description = childModel.Description;
                    existingChild.Dimensions = childModel.Dimensions;
                    existingChild.Director = childModel.Director;
                    existingChild.Downloadable = childModel.Downloadable;
                    existingChild.Expiry = childModel.Expiry;
                    existingChild.Format = childModel.Format;
                    existingChild.Hall = childModel.Hall;
                    existingChild.HallPart = childModel.HallPart;
                    existingChild.HotelStars = childModel.HotelStars;
                    existingChild.Included = childModel.Included;
                    existingChild.IsKids = childModel.IsKids;
                    existingChild.IsPremiere = childModel.IsPremiere;
                    existingChild.Language = childModel.Language;
                    existingChild.ManufacturerWarranty = childModel.ManufacturerWarranty;
                    existingChild.Meal = childModel.Meal;
                    existingChild.Media = childModel.Media;
                    existingChild.Model = childModel.Model;
                    existingChild.Name = childModel.Name;
                    existingChild.OldPrice = childModel.OldPrice;
                    existingChild.Options = childModel.Options;
                    existingChild.OriginalName = childModel.OriginalName;
                    existingChild.PageExtent = childModel.PageExtent;
                    existingChild.Part = childModel.Part;
                    existingChild.PerformanceType = childModel.PerformanceType;
                    existingChild.PerformedBy = childModel.PerformedBy;
                    existingChild.Pickup = childModel.Pickup;
                    existingChild.Place = childModel.Place;
                    existingChild.PriceMax = childModel.PriceMax;
                    existingChild.PriceMin = childModel.PriceMin;
                    existingChild.Publisher = childModel.Publisher;
                    existingChild.PurchasePrice = childModel.PurchasePrice;
                    existingChild.RecordingLength = childModel.RecordingLength;
                    existingChild.Region = childModel.Region;
                    existingChild.Room = childModel.Room;
                    existingChild.SalesNotes = childModel.SalesNotes;
                    existingChild.Series = childModel.Series;
                    existingChild.Starring = childModel.Starring;
                    existingChild.Storage = childModel.Storage;
                    existingChild.Store = childModel.Store;
                    existingChild.TableOfContents = childModel.TableOfContents;
                    existingChild.Title = childModel.Title;
                    existingChild.Transport = childModel.Transport;
                    existingChild.TypePrefix = childModel.TypePrefix;
                    existingChild.Url = childModel.Url;
                    existingChild.Vat = childModel.Vat;
                    existingChild.Vendor = childModel.Vendor;
                    existingChild.VendorCode = childModel.VendorCode;
                    existingChild.Volume = childModel.Volume;
                    existingChild.Weight = childModel.Weight;
                    existingChild.WorldRegion = childModel.WorldRegion;
                    existingChild.Year = childModel.Year;
                    existingChild.Id = childModel.Id;
                    existingChild.GroupId = childModel.GroupId;
                    existingChild.Type = childModel.Type;
                    existingChild.Available = childModel.Available;
                    existingChild.Bid = childModel.Bid;
                    existingChild.Cbid = childModel.Cbid;
                    existingChild.Fee = childModel.Fee;

                    unitOfWork.ObjectStateManager.GetObjectStateEntry(existingChild)
                        .ChangeState(EntityState.Modified);

                    UpdateOfferAge(unitOfWork, existingChild, childModel);
                    UpdateOfferCategoryId(unitOfWork, existingChild, childModel);
                    UpdateOfferPrice(unitOfWork, existingChild, childModel);

                    UpdateOfferBarcodes(unitOfWork, existingChild, childModel);
                    UpdateOfferDeliveryOptions(unitOfWork, existingChild, childModel);
                    UpdateOfferDataTours(unitOfWork, existingChild, childModel);
                    UpdateOfferParams(unitOfWork, existingChild, childModel);
                    UpdateOfferPictures(unitOfWork, existingChild, childModel);
                    UpdateOfferPricelabsParams(unitOfWork, existingChild, childModel);
                }
                else
                {
                    _log.InfoFormat("Добавление предложения магазина: {0}", childModel.Id);
                    shop.Offers.Add(childModel);
                }
            }
        }

        private void UpdateOfferAge(IUnitOfWork unitOfWork, YmlOffer offer, YmlOffer newOffer)
        {
            //_log.InfoFormat("Обновление возрастной категории. Предложение({0})...", offer.Id);

            if (newOffer.Age == null)
            {
                offer.Age = null;
            }
            else if(offer.Age ==  null)
            {
                offer.Age = newOffer.Age;
            }
            else
            {
                offer.Age.Unit = newOffer.Age.Unit;
                offer.Age.Value = newOffer.Age.Value;

                unitOfWork.ObjectStateManager.GetObjectStateEntry(offer.Age)
                    .ChangeState(EntityState.Modified);

            }
        }

        private void UpdateOfferCategoryId(IUnitOfWork unitOfWork, YmlOffer offer, YmlOffer newOffer)
        {
            //_log.InfoFormat("Обновление идентификатора категории товара. Предложение({0})...", offer.Id);

            if (newOffer.CategoryId == null)
            {
                offer.CategoryId = null;
            }
            else if (offer.CategoryId == null)
            {
                offer.CategoryId = newOffer.CategoryId;
            }
            else
            {
                offer.CategoryId.Type = newOffer.CategoryId.Type;
                offer.CategoryId.Value = newOffer.CategoryId.Value;

                unitOfWork.ObjectStateManager.GetObjectStateEntry(offer.CategoryId)
                    .ChangeState(EntityState.Modified);
            }
        }

        private void UpdateOfferPrice(IUnitOfWork unitOfWork, YmlOffer offer, YmlOffer newOffer)
        {
            //_log.InfoFormat("Обновление цены товара. Предложение({0})...", offer.Id);

            if (newOffer.Price == null)
            {
                offer.Price = null;
            }
            else if (offer.Price == null)
            {
                offer.Price = newOffer.Price;
            }
            else
            {
                offer.Price.From = newOffer.Price.From;
                offer.Price.Value = newOffer.Price.Value;

                unitOfWork.ObjectStateManager.GetObjectStateEntry(offer.Price)
                    .ChangeState(EntityState.Modified);
            }
        }

        private void UpdateOfferDataTours(IUnitOfWork unitOfWork, YmlOffer offer, YmlOffer newOffer)
        {
            //_log.InfoFormat("Обновление дат заездов. Предложение({0})...", offer.Id);

            foreach (var dataTour in offer.DataTours.ToArray())
            {
                if (!newOffer.DataTours.Any(c => c.Value.Equals(dataTour.Value)))
                {
                    unitOfWork.ObjectContext.DeleteObject(dataTour);
                }
            }

            foreach (var childModel in newOffer.DataTours)
            {
                var existingChild = offer.DataTours
                    .SingleOrDefault(c => c.Value.Equals(childModel.Value));

                if (existingChild != null)
                {
                    existingChild.Value = childModel.Value;

                    unitOfWork.ObjectStateManager.GetObjectStateEntry(existingChild)
                        .ChangeState(EntityState.Modified);
                }
                else
                {
                    offer.DataTours.Add(childModel);
                }
            }
        }

        private void UpdateOfferDeliveryOptions(IUnitOfWork unitOfWork, YmlOffer offer, YmlOffer newOffer)
        {
            //_log.InfoFormat("Обновление условий курьерской доставки товара. Предложение({0})...", offer.Id);

            foreach (var deliveryOption in offer.DeliveryOptions.ToArray())
            {
                if (!newOffer.DeliveryOptions.Any(c => c.Cost.Equals(deliveryOption.Cost)
                                                         && c.Days.Equals(deliveryOption.Days)
                                                         && c.OrderBefore.Equals(deliveryOption.OrderBefore)))
                {
                    unitOfWork.ObjectContext.DeleteObject(deliveryOption);
                }
            }

            foreach (var childModel in newOffer.DeliveryOptions)
            {
                var existingChild = offer.DeliveryOptions
                    .SingleOrDefault(c => c.Cost.Equals(childModel.Cost)
                                          && c.Days.Equals(childModel.Days)
                                          && c.OrderBefore.Equals(childModel.OrderBefore));

                if (existingChild != null)
                {
                    existingChild.Cost = childModel.Cost;
                    existingChild.Days = childModel.Days;
                    existingChild.OrderBefore = childModel.OrderBefore;

                    unitOfWork.ObjectStateManager.GetObjectStateEntry(existingChild)
                        .ChangeState(EntityState.Modified);
                }
                else
                {
                    offer.DeliveryOptions.Add(childModel);
                }
            }
        }
        
        private void UpdateOfferBarcodes(IUnitOfWork unitOfWork, YmlOffer offer, YmlOffer newOffer)
        {
            //_log.InfoFormat("Обновление штрихкодов товара. Предложение({0})...", offer.Id);

            foreach (var barcode in offer.Barcodes.ToArray())
            {
                if (!newOffer.Barcodes.Any(c => c.Value.Equals(barcode.Value)))
                {
                    unitOfWork.ObjectContext.DeleteObject(barcode);
                }
            }

            foreach (var childModel in newOffer.Barcodes)
            {
                var existingChild = offer.Barcodes
                    .SingleOrDefault(c => c.Value.Equals(childModel.Value));

                if (existingChild != null)
                {
                    existingChild.Value = childModel.Value;

                    unitOfWork.ObjectStateManager.GetObjectStateEntry(existingChild)
                        .ChangeState(EntityState.Modified);
                }
                else
                {
                    offer.Barcodes.Add(childModel);
                }
            }
        }

        private void UpdateOfferParams(IUnitOfWork unitOfWork, YmlOffer offer, YmlOffer newOffer)
        {
           // _log.InfoFormat("Обновление характеристик товара. Предложение({0})...", offer.Id);

            foreach (var param in offer.Params.ToArray())
            {
                if (!newOffer.Params.Any(c => c.Name.Equals(param.Name)))
                {
                    unitOfWork.ObjectContext.DeleteObject(param);
                }
            }

            foreach (var childModel in newOffer.Params)
            {
                var existingChild = offer.Params
                    .SingleOrDefault(c => c.Name.Equals(childModel.Name));

                if (existingChild != null)
                {
                    existingChild.Name = childModel.Name;
                    existingChild.Value = childModel.Value;
                    existingChild.Unit = childModel.Unit;

                    unitOfWork.ObjectStateManager.GetObjectStateEntry(existingChild)
                        .ChangeState(EntityState.Modified);
                }
                else
                {
                    offer.Params.Add(childModel);
                }
            }
        }

        private void UpdateOfferPictures(IUnitOfWork unitOfWork, YmlOffer offer, YmlOffer newOffer)
        {
            //_log.InfoFormat("Обновление URL ссылок на картинки товара. Предложение({0})...", offer.Id);

            foreach (var picture in offer.Pictures.ToArray())
            {
                if (!newOffer.Pictures.Any(c => c.Url.Equals(picture.Url)))
                {
                    unitOfWork.ObjectContext.DeleteObject(picture);
                }
            }

            foreach (var childModel in newOffer.Pictures)
            {
                var existingChild = offer.Pictures
                    .SingleOrDefault(c => c.Url.Equals(childModel.Url));

                if (existingChild != null)
                {
                    existingChild.Url = childModel.Url;

                    unitOfWork.ObjectStateManager.GetObjectStateEntry(existingChild)
                        .ChangeState(EntityState.Modified);
                }
                else
                {
                    offer.Pictures.Add(childModel);
                }
            }
        }

        private void UpdateOfferPricelabsParams(IUnitOfWork unitOfWork, YmlOffer offer, YmlOffer newOffer)
        {
            //_log.InfoFormat("Обновление параметров для PriceLabs. Предложение({0})...", offer.Id);

            foreach (var param in offer.PricelabsParams.ToArray())
            {
                if (!newOffer.PricelabsParams.Any(c => c.Name.Equals(param.Name)))
                {
                    unitOfWork.ObjectContext.DeleteObject(param);
                }
            }

            foreach (var childModel in newOffer.PricelabsParams)
            {
                var existingChild = offer.PricelabsParams
                    .SingleOrDefault(c => c.Name.Equals(childModel.Name));

                if (existingChild != null)
                {
                    existingChild.Name = childModel.Name;
                    existingChild.Value = childModel.Value;

                    unitOfWork.ObjectStateManager.GetObjectStateEntry(existingChild)
                        .ChangeState(EntityState.Modified);
                }
                else
                {
                    offer.PricelabsParams.Add(childModel);
                }
            }
        }

        private void UpdateShopPromos(IUnitOfWork unitOfWork, YmlShop shop, YmlShop newShop)
        {
            _log.InfoFormat("Обновление информации о промоакциях...");

            foreach (var existingChild in shop.Promos.ToArray())
            {
                if (!newShop.Promos.Any(
                    u => u.Id.Equals(existingChild.Id)))
                {
                    unitOfWork.ObjectContext.DeleteObject(existingChild);
                }
            }

            foreach (var childModel in newShop.Promos)
            {
                var existingChild = shop.Promos
                    .SingleOrDefault(c => c.Id.Equals(childModel.Id));

                if (existingChild != null)
                {
                    _log.InfoFormat("Обновление информации о промоакции: {0}", childModel.Id);

                    existingChild.Id = childModel.Id;
                    existingChild.Description = childModel.Description;
                    existingChild.EndDate = childModel.EndDate;
                    existingChild.PromoCode = childModel.PromoCode;
                    existingChild.StartDate = childModel.StartDate;
                    existingChild.Type = childModel.Type;
                    existingChild.Url = childModel.Url;

                    unitOfWork.ObjectStateManager.GetObjectStateEntry(existingChild)
                        .ChangeState(EntityState.Modified);

                    UpdatePromoDiscount(unitOfWork, existingChild, childModel);
                    UpdatePromoGifts(unitOfWork, existingChild, childModel);
                    UpdatePromoPurchase(unitOfWork, existingChild, childModel);
                }
                else
                {
                    _log.InfoFormat("Добавление информации о промоакциии: {0}", childModel.Id);

                    shop.Promos.Add(childModel);
                }
            }
        }

        private void UpdatePromoDiscount(IUnitOfWork unitOfWork, YmlPromo promo, YmlPromo newPromo)
        {
            if (newPromo.Discount == null)
            {
                promo.Discount = null;
            }
            else if (promo.Discount == null)
            {
                promo.Discount = newPromo.Discount;
            }
            else
            {
                promo.Discount.Currency = newPromo.Discount.Currency;
                promo.Discount.Unit = newPromo.Discount.Unit;
                promo.Discount.Value = newPromo.Discount.Value;

                unitOfWork.ObjectStateManager.GetObjectStateEntry(promo.Discount)
                    .ChangeState(EntityState.Modified);
            }
        }

        private void UpdatePromoGifts(IUnitOfWork unitOfWork, YmlPromo promo, YmlPromo newPromo)
        {
            foreach (var promoGift in promo.PromoGifts.ToArray())
            {
                if (!newPromo.PromoGifts.Any(c => (c.GiftId!=null && c.GiftId.Equals(promoGift.GiftId))
                || (c.OfferId != null && c.OfferId.Equals(promoGift.OfferId))))
                {
                    unitOfWork.ObjectContext.DeleteObject(promoGift);
                }
            }

            foreach (var childModel in newPromo.PromoGifts)
            {
                YmlPromoGift existingChild = null;

                if (childModel.GiftId != null)
                {
                    existingChild = promo.PromoGifts
                        .SingleOrDefault(c => c.GiftId != null && c.GiftId.Equals(childModel.GiftId));
                }
                else if (childModel.OfferId != null)
                {
                    existingChild = promo.PromoGifts
                        .SingleOrDefault(c => c.OfferId!=null && c.OfferId.Equals(childModel.OfferId));
                }

                if (existingChild != null)
                {
                    existingChild.GiftId = childModel.GiftId;
                    existingChild.OfferId = childModel.OfferId;

                    unitOfWork.ObjectStateManager.GetObjectStateEntry(existingChild)
                        .ChangeState(EntityState.Modified);
                }
                else
                {
                    promo.PromoGifts.Add(childModel);
                }
            }
        }

        private void UpdatePromoPurchase(IUnitOfWork unitOfWork, YmlPromo promo, YmlPromo newPromo)
        {
            if (newPromo.Purchase == null)
            {
                promo.Purchase = null;
            }
            else if (promo.Purchase == null)
            {
                promo.Purchase = newPromo.Purchase;
            }
            else
            {
                promo.Purchase.FreeQuantity = newPromo.Purchase.FreeQuantity;
                promo.Purchase.RequiredQuantity = newPromo.Purchase.RequiredQuantity;

                unitOfWork.ObjectStateManager.GetObjectStateEntry(promo.Purchase)
                    .ChangeState(EntityState.Modified);

                UpdatePurchaseProducts(unitOfWork, promo.Purchase, newPromo.Purchase);
            }
        }

        private void UpdatePurchaseProducts(IUnitOfWork unitOfWork, YmlPurchase purchase, YmlPurchase newPurchase)
        {
            foreach (var existingChild in purchase.Products.ToArray())
            {
                if (!newPurchase.Products.Any(
                    u => u.OfferId.Equals(existingChild.OfferId)))
                {
                    unitOfWork.ObjectContext.DeleteObject(existingChild);
                }
            }

            foreach (var childModel in newPurchase.Products)
            {
                var existingChild = purchase.Products
                    .SingleOrDefault(c => c.OfferId.Equals(childModel.OfferId));

                if (existingChild != null)
                {
                    existingChild.CategoryId = childModel.CategoryId;
                    existingChild.OfferId = childModel.OfferId;

                    unitOfWork.ObjectStateManager.GetObjectStateEntry(existingChild)
                        .ChangeState(EntityState.Modified);

                    UpdateProductDiscountPrice(unitOfWork, existingChild, childModel);
                }
                else
                {
                    purchase.Products.Add(childModel);
                }
            }
        }

        private void UpdateProductDiscountPrice(IUnitOfWork unitOfWork, YmlProduct product, YmlProduct newProduct)
        {
            if (newProduct.DiscountPrice == null)
            {
                product.DiscountPrice = null;
            }
            else if (product.DiscountPrice == null)
            {
                product.DiscountPrice = newProduct.DiscountPrice;
            }
            else
            {
                product.DiscountPrice.Currency = newProduct.DiscountPrice.Currency;
                product.DiscountPrice.Value = newProduct.DiscountPrice.Value;

                unitOfWork.ObjectStateManager.GetObjectStateEntry(product.DiscountPrice)
                    .ChangeState(EntityState.Modified);
            }
        }
    }
}
