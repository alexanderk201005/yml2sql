﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Yml2SQL.Interfaces;
using YmlDb;

namespace Yml2SQL.Code
{
    public class GenericRepository<T> : IRepository<T> where T : class
    {
        private readonly YmlDbContext _dbContext;

        private IDbSet<T> DbSet => _dbContext.Set<T>();

        public IQueryable<T> Entities => DbSet;

        public GenericRepository(YmlDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Remove(T entity)
        {
            DbSet.Remove(entity);
        }

        public void Add(T entity)
        {
            DbSet.Add(entity);
        }

        public void Update(T entity, bool submitImmediately = false)
        {
            var key = GetKeyValue(entity);

            var originalEntity = DbSet.Find(key);

            _dbContext.Entry(originalEntity).CurrentValues.SetValues(entity);

            if (submitImmediately)
            {
                _dbContext.SaveChanges();
            }
        }

        public object GetKeyValue(T t)
        {
            var key =
                typeof(T).GetProperties().FirstOrDefault(
                    p => p.GetCustomAttributes(
                             typeof(System.ComponentModel.DataAnnotations.KeyAttribute), true)
                             .Length != 0);
            return key?.GetValue(t, null);

        }

        public void RejectChanges()
        {
            foreach (var entry in _dbContext.ChangeTracker.Entries()
                .Where(e => e.State != EntityState.Unchanged))
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                    case EntityState.Modified:
                    case EntityState.Deleted:
                        entry.Reload();
                        break;
                }
            }
        }

        public void Attach(T entity)
        {
            DbSet.Attach(entity);
        }
    }
}
