﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using AutoMapper;
using log4net;
using Yml2SQL.Helpers;
using Yml2SQL.Interfaces;
using YmlModel.Model;

namespace Yml2SQL.Code
{
    public class YmlDataBinder : IYmlDataBinder
    {
        private readonly ILog _log;

        public YmlDataBinder(ILog log)
        {
            _log = log;
        }

        public YmlCatalog CreateCatalog(MemoryStream stream)
        {
            _log.Info("Создание каталога...");

            try
            {
                var reader = new StreamReader(stream);
                stream.Position = 0;
                var xmlString = reader.ReadLine();

                var encoding = XmlHelpers.GetXmlEncoding(xmlString);

                stream.Position = 0;
                using (var src = new StreamReader(stream, encoding))
                {
                    var serializer = new XmlSerializer(typeof(yml_catalog));

                    var catalog = serializer.Deserialize(src) as yml_catalog;

                    var result = Mapper.Map<yml_catalog, YmlCatalog>(catalog);

                    _log.Info("Создание каталога завершено.");

                    return result;
                }
            }
            catch (Exception ex)
            {
                _log.ErrorFormat("Ошибка создания каталога:{0}", ex.Message);
            }

            return null;
        }
    }
}
