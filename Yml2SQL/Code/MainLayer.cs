﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using log4net;
using Yml2SQL.Interfaces;
using Yml2SQL.Properties;
using YmlModel.Model;

namespace Yml2SQL.Code
{
    public class MainLayer : IMainLayer
    {
        private readonly ILog _log;
        private readonly IYmlDataBinder _dataBinder;
        private readonly IDataReader _dataReader;
        private readonly IDataSaver _dataSaver;

        public MainLayer(ILog log, IYmlDataBinder dataBinder, IDataReader dataReader,
            IDataSaver dataSaver)
        {
            _log = log;
            _dataBinder = dataBinder;
            _dataReader = dataReader;
            _dataSaver = dataSaver;
        }

        public void Run(bool isNetwork)
        {
            var baseUrl = Settings.Default.baseUrl;
            var resource = Settings.Default.resource;
            var file = Settings.Default.ymlFile;

            var stream = isNetwork ? _dataReader.GetData(baseUrl, resource) 
                : _dataReader.GetData("..\\..\\..\\ymls\\", file);

            var catalog = _dataBinder.CreateCatalog(stream);

            if (catalog != null)
            {
                _dataSaver.SaveCatalog(catalog);
            }
        }

    }
}
