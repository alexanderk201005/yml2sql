﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using RestSharp;
using Yml2SQL.Interfaces;

namespace Yml2SQL.Code
{
    public class NetworkLayer : INetworkLayer
    {
        private readonly ILog _log;

        public NetworkLayer(ILog log)
        {
            _log = log;
        }

        public MemoryStream Download(string baseUrl, string resource)
        {
            var client = new RestClient
            {
                BaseUrl = new Uri(baseUrl)
            };

            var request = new RestRequest
            {
                Resource = resource,
                Method = Method.GET

            };
            var response = client.DownloadData(request, true);

            return new MemoryStream(response);
        }
    }
}
