﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Yml2SQL.Interfaces;

namespace Yml2SQL.Code
{
    public class NetworkDataReader : IDataReader
    {
        private readonly ILog _log;
        private readonly INetworkLayer _networkLayer;

        public NetworkDataReader(ILog log, INetworkLayer networkLayer)
        {
            _log = log;
            _networkLayer = networkLayer;
        }

        public MemoryStream GetData(string baserUrl, string resource)
        {
            var uri = new Uri(new Uri(baserUrl), resource);
            _log.InfoFormat("Загрузка {0}...", uri.AbsoluteUri);

            var stream = _networkLayer.Download(baserUrl, resource);

            _log.InfoFormat("Загрузка {0} завершена.", uri.AbsoluteUri);

            return stream;
        }
    }
}
