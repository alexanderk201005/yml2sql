﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Yml2SQL.Interfaces;

namespace Yml2SQL.Code
{
    public class FileDataReader : IDataReader
    {
        private readonly ILog _log;

        public FileDataReader(ILog log)
        {
            _log = log;
        }

        public MemoryStream GetData(string baserUrl, string resource)
        {
            var fullPath = Path.Combine(baserUrl, resource);
            _log.InfoFormat("Чтение файла {0}...", fullPath);

            var ms = new MemoryStream();
            using (var file = new FileStream(fullPath, FileMode.Open, FileAccess.Read))
            {
                file.CopyTo(ms);
            }

            _log.InfoFormat("Чтение файла {0} завершено.", fullPath);

            return ms;
        }
    }
}
