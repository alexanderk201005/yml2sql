﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Autofac.log4net;
using log4net;
using log4net.Config;
using log4net.Core;
using Yml2SQL.Code;
using Yml2SQL.Configurations;
using Yml2SQL.Interfaces;

namespace Yml2SQL
{
    class Program
    {
        private static IContainer Container { get; set; }
        private static ILog Logger { get; set; }

        static void Main(string[] args)
        {
            try
            {
                XmlConfigurator.Configure();

                Logger = LogManager.GetLogger("Logger");

                Logger.InfoFormat("{0} v.{1} запущен.", Assembly.GetExecutingAssembly().GetName().Name, Assembly.GetExecutingAssembly().GetName().Version.ToString());

                var isNetwork = IsNetworkMode();

                var builder = new ContainerBuilder();

                var loggingModule = new Log4NetModule();
                loggingModule.MapTypeToLoggerName(typeof(FileDataReader), "Logger");
                loggingModule.MapTypeToLoggerName(typeof(MainLayer), "Logger");
                loggingModule.MapTypeToLoggerName(typeof(NetworkDataReader), "Logger");
                loggingModule.MapTypeToLoggerName(typeof(NetworkLayer), "Logger");
                loggingModule.MapTypeToLoggerName(typeof(YmlDataBinder), "Logger");
                loggingModule.MapTypeToLoggerName(typeof(YmlDbDataSaver), "Logger");
                builder.RegisterModule(loggingModule);

                if (isNetwork)
                {
                    builder.RegisterType<NetworkDataReader>().As<IDataReader>().SingleInstance();
                }
                else
                {
                    builder.RegisterType<FileDataReader>().As<IDataReader>().SingleInstance();
                }

                builder.RegisterType<MainLayer>().As<IMainLayer>().SingleInstance();
                builder.RegisterType<NetworkLayer>().As<INetworkLayer>().SingleInstance();
                builder.RegisterType<YmlDataBinder>().As<IYmlDataBinder>().SingleInstance();
                builder.RegisterType<YmlDbDataSaver>().As<IDataSaver>().SingleInstance();

                AutoMapperConfiguration.Configure();

                Container = builder.Build();
                
                var mainLayer = Container.Resolve<IMainLayer>();

                mainLayer.Run(isNetwork);

            }
            catch (Exception ex)
            {
                Console.WriteLine(@"ОШИБКА: '{0}'", ex.Message);
            }

            Console.WriteLine(@"Нажмите любую клавишу для выхода...");
            Console.ReadKey();
        }

        private static bool IsNetworkMode()
        {
            Logger.InfoFormat("Загрузка из сети?(Y-да/Иначе из файла):");

            var input = Console.ReadLine();

            return input != null && input.ToUpper() =="Y";
        }
    }
}
