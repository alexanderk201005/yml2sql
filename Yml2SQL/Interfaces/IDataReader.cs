﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yml2SQL.Interfaces
{
    public interface IDataReader
    {
        MemoryStream GetData(string baserUrl, string resource);
    }
}
