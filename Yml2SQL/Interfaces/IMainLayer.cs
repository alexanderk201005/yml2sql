﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yml2SQL.Interfaces
{
    public interface IMainLayer
    {
        void Run(bool isNetwork);
    }
}
