﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YmlModel.Model;

namespace Yml2SQL.Interfaces
{
    public interface IDataSaver
    {
        void SaveCatalog(YmlCatalog catalog);
    }
}
