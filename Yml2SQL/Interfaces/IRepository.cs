﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Yml2SQL.Interfaces
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> Entities { get; }

        void Remove(T entity);
        void Add(T entity);
        void Update(T entity, bool submitImmediately = false);

        void RejectChanges();

        void Attach(T entity);

    }
}
