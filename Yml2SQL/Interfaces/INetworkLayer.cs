﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace Yml2SQL.Interfaces
{
    public interface INetworkLayer
    {
        MemoryStream Download(string baseUrl, string resource);
    }
}
