﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YmlModel.Model;

namespace Yml2SQL.Interfaces
{
    public interface IYmlDataBinder
    {
        YmlCatalog CreateCatalog(MemoryStream stream);
    }
}
