﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Yml2SQL.Helpers
{
    public static class XmlHelpers
    {
        public static Encoding GetXmlEncoding(string xmlString)
        {
            using (var stringReader = new StringReader(xmlString))
            {
                var settings = new XmlReaderSettings { ConformanceLevel = ConformanceLevel.Fragment };

                var reader = XmlReader.Create(stringReader, settings);
                reader.Read();

                var encoding = reader.GetAttribute("encoding");

                var result = Encoding.GetEncoding(encoding ?? throw new InvalidOperationException());
                return result;
            }
        }
    }
}
