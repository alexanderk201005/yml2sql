﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

/// <summary>
/// Корневой элемент XML-документа.
/// </summary>
[GeneratedCode("xsd", "4.7.3062.0")]
[Serializable]
[DebuggerStepThrough]
[DesignerCategory("code")]
[XmlType(AnonymousType = true)]
[XmlRoot(Namespace = "", IsNullable = false)]
public class yml_catalog
{
    /// <summary>
    ///  Элемент содержит описание магазина (shop) и его предложений (offers):
    /// </summary>
    public yml_shop shop;


    /// <summary>
    /// Дате и время генерации XML-файла на стороне магазина.
    /// Дата должна иметь формат YYYY-MM-DD HH:mm.
    /// </summary>
    [XmlAttribute]
    public string date;
}

/// <summary>
/// Описание магазина и его товарных предложений.
/// </summary>
[GeneratedCode("xsd", "4.7.3062.0")]
[Serializable]
[DebuggerStepThrough]
[DesignerCategory("code")]
[XmlType(AnonymousType = true)]
[XmlRoot(Namespace = "", IsNullable = false)]
public class yml_shop
{
    // NOTE: Не найдено в описании от 23 мая 2018 г.
    // https://yandex.ru/support/market-tech-requirements/index.html
    // public string adult;

    /// <summary>
    /// Наименование агентства, которое оказывает техническую поддержку магазину
    /// и отвечает за работоспособность сайта.
    /// 
    /// Необязательный элемент.
    /// </summary>
    public string agency;

    /// <summary>
    /// Список категорий магазина.
    /// 
    /// Обязательный элемент.
    /// </summary>
    [XmlArrayItem("category", IsNullable = false)]
    public yml_category[] categories;

    /// <summary>
    /// Полное наименование компании, владеющей магазином. Не публикуется,
    /// используется для внутренней идентификации.
    /// 
    /// Обязательный элемент.
    /// </summary>
    public string company;

    // NOTE: Программа «Заказ на Маркете» закрыта.
    /// <summary>
    /// Признак участия товарного предложение в проекте «Покупка на Маркете»
    /// </summary>
    // public string cpa;

    /// <summary>
    /// Список курсов валют магазина.
    /// 
    /// Обязательный элемент.
    /// </summary>
    [XmlArrayItem("currency", IsNullable = false)]
    public yml_currency[] currencies;

    // NOTE: Не найдено в описании от 23 мая 2018 г.
    // https://yandex.ru/support/market-tech-requirements/index.html
    // public yml_delivery delivery;

    /// <summary>
    /// Стоимость и сроки курьерской доставки по региону, в котором находится магазин.
    /// 
    /// Обязательный элемент, если все данные по доставке передаются в прайс-листе.
    /// </summary>
    [XmlArray("delivery-options")]
    [XmlArrayItem("option", IsNullable = false)]
    public yml_deliveryOption[] Delivery;

    // NOTE: Не найдено в описании от 23 мая 2018 г.
    // https://yandex.ru/support/market-tech-requirements/index.html
    // public bool deliveryIncluded;

    /// <summary>
    /// Контактный адрес разработчиков CMS или агентства, осуществляющего техподдержку.
    /// 
    /// Необязательный элемент.
    /// </summary>
    public string email;

    // NOTE: Не найдено в описании от 23 мая 2018 г.
    // https://yandex.ru/support/market-tech-requirements/index.html
    //public string fee;

    // NOTE: Не найдено в описании от 23 мая 2018 г.
    // https://yandex.ru/support/market-tech-requirements/index.html
    //public string local_delivery_cost;

    /// <summary>
    /// Короткое название магазина, не более 20 символов.
    /// В названии нельзя использовать слова, не имеющие отношения к наименованию магазина,
    /// например «лучший», «дешевый», указывать номер телефона и т. п.
    /// Название магазина должно совпадать с фактическим названием магазина,
    /// которое публикуется на сайте.При несоблюдении этого требования наименование
    /// Яндекс.Маркет может самостоятельно изменить название без уведомления магазина.
    /// 
    /// Обязательный элемент.
    /// </summary>
    public string name;

    // NOTE: Не найдено в описании от 23 мая 2018 г.
    // https://yandex.ru/support/market-tech-requirements/index.html
    //public string phone;

    // NOTE: Не найдено в описании от 23 мая 2018 г.
    // https://yandex.ru/support/market-tech-requirements/index.html
    //public string pickup;

    /// <summary>
    /// Система управления контентом, на основе которой работает магазин (CMS).
    /// 
    /// Необязательный элемент.
    /// </summary>
    public string platform;

    // NOTE: Не найдено в описании от 23 мая 2018 г.
    // https://yandex.ru/support/market-tech-requirements/index.html
    //public string store;

    /// <summary>
    /// URL главной страницы магазина. Максимум 50 символов. Допускаются кириллические ссылки.
    /// </summary>
    public string url;

    /// <summary>
    /// Версия CMS.
    /// 
    /// Необязательный элемент.
    /// </summary>
    public string version;

    /// <summary>
    /// Список предложений магазина. Каждое предложение описывается в отдельном элементе offer.
    /// Здесь не приводится список всех элементов, входящих в offer,
    /// так как он зависит от типа предложения.
    /// Для большинства категорий товаров подходят следующие типы описаний:
    ///     Упрощенный тип
    ///     Произвольный тип
    /// Для некоторых категорий товаров нужно использовать собственные типы описаний:
    ///     Лекарства
    ///     Книги
    ///     Аудиокниги
    ///     Музыкальная и видеопродукция
    ///     Билеты на мероприятия
    ///     Туры
    /// 
    /// Обязательный элемент.
    /// </summary>
    [XmlArrayItem("offer", IsNullable = false)]
    public yml_offer[] offers;

    /// <summary>
    /// Подарки, которые не размещаются на Маркете (для акции "Подарок на выбор").
    /// </summary>
    [XmlArrayItem("gift", IsNullable = false)]
    public yml_gift[] gifts;

    /// <summary>
    /// Промоакции.
    /// </summary>
    [XmlArrayItem("promo", IsNullable = false)]
    public yml_promo[] promos;
}

/// <summary>
/// Категория товара.
/// </summary>
[GeneratedCode("xsd", "4.7.3062.0")]
[Serializable]
[DebuggerStepThrough]
[DesignerCategory("code")]
[XmlType(AnonymousType = true)]
[XmlRoot(Namespace = "", IsNullable = false)]
public class yml_category
{
    /// <summary>
    /// Идентификатор категории.
    /// </summary>
    [XmlAttribute]
    public string id;

    /// <summary>
    /// Идентификатор категории более высокого уровня — parentId (для подкатегорий).
    /// </summary>
    [XmlAttribute]
    public string parentId;

    // NOTE: Не найдено в описании от 23 мая 2018 г.
    // https://yandex.ru/support/market-tech-requirements/index.html
    // [System.Xml.Serialization.XmlAttributeAttribute()]
    // public string tid;

    // NOTE: Не найдено в описании от 23 мая 2018 г.
    // https://yandex.ru/support/market-tech-requirements/index.html
    // [System.Xml.Serialization.XmlAttributeAttribute()]
    // public string yid;

    /// <summary>
    /// Название категории.
    /// </summary>
    [XmlText]
    public string Value;
}

/// <summary>
/// Курс валюты.
/// </summary>
[GeneratedCode("xsd", "4.7.3062.0")]
[Serializable]
[DebuggerStepThrough]
[DesignerCategory("code")]
[XmlType(AnonymousType = true)]
[XmlRoot(Namespace = "", IsNullable = false)]
public class yml_currency
{
    /// <summary>
    /// Код одной или нескольких валют.
    /// </summary>
    [XmlAttribute]
    public yml_currencyID id;

    /// <summary>
    /// Указывает курс валюты к курсу основной валюты,
    /// взятой за единицу (валюта, для которой rate="1"). Параметр rate может иметь следующие значения:
    ///     Постоянное число — внутренний курс, который вы используете.
    ///     CBRF — курс по Центральному банку РФ.
    ///     NBU — курс по Национальному банку Украины.
    ///     NBK — курс по Национальному банку Казахстана.
    ///     СВ — курс по банку той страны, к которой относится магазин по своему региону,
    ///          указанному в личном кабинете.
    /// В качестве основной валюты (для которой установлено rate= "1") могут быть использованы
    /// только рубль(RUR, RUB), белорусский рубль(BYN), гривна(UAH) или тенге(KZT).
    /// </summary>
    [XmlAttribute]
    [DefaultValue("1")]
    public string rate;

    /// <summary>
    /// % процент прибавляемый к курсу.
    /// </summary>
    [XmlAttribute]
    [DefaultValue("0")]
    public string plus;

    /// <summary>
    /// Initializes a new instance of the <see cref="yml_currency"/> class.
    /// </summary>
    public yml_currency()
    {
        rate = "1";
        plus = "0";
    }
}

/// <summary>
/// Идентификатор валюты товара.
/// </summary>
[GeneratedCode("xsd", "4.7.3062.0")]
[Serializable]
[XmlType(AnonymousType = true)]
public enum yml_currencyID
{

    /// <remarks/>
    RUR,

    /// <remarks/>
    RUB,

    /// <remarks/>
    USD,

    /// <remarks/>
    BYR,

    /// <remarks/>
    BYN,

    /// <remarks/>
    KZT,

    /// <remarks/>
    EUR,

    /// <remarks/>
    UAH
}

/// <remarks/>
[GeneratedCode("xsd", "4.7.3062.0")]
[Serializable]
[DebuggerStepThrough]
[DesignerCategory("code")]
[XmlType(AnonymousType = true)]
[XmlRoot(Namespace = "", IsNullable = false)]
public class yml_delivery
{
    /// <remarks/>
    [XmlAttribute]
    public string days;

    /// <remarks/>
    [XmlText]
    public string Value;
}

/// <summary>
/// Cтоимость и сроки курьерской доставки по вашему региону. 
/// </summary>
[GeneratedCode("xsd", "4.7.3062.0")]
[Serializable]
[DebuggerStepThrough]
[DesignerCategory("code")]
[XmlType(AnonymousType = true)]
public class yml_deliveryOption
{
    /// <summary>
    /// Стоимость доставки.
    /// </summary>
    [XmlAttribute]
    public string cost;

    /// <summary>
    /// Срок доставки в рабочих днях.
    /// </summary>
    [XmlAttribute]
    public string days;

    /// <summary>
    /// Время, до которого нужно сделать заказ, чтобы получить его в этот срок.
    /// 
    /// Необязательный элемент.
    /// </summary>
    [XmlAttribute("order-before")]
    public string orderbefore;
}

/// <summary>
/// Предложение магазина.
/// </summary>
[GeneratedCode("xsd", "4.7.3062.0")]
[Serializable]
[DebuggerStepThrough]
[DesignerCategory("code")]
[XmlType(AnonymousType = true)]
[XmlRoot(Namespace = "", IsNullable = false)]
public class yml_offer
{
    /// <summary>
    /// International Standard Book Number — международный уникальный номер книжного издания.
    /// Если их несколько, указываются через запятую.
    /// </summary>
    public string ISBN;

    // NOTE: Не найдено в описании от 23 мая 2018 г.
    // https://yandex.ru/support/market-tech-requirements/index.html
    // public string additional;

    /// <summary>
    /// Отношение товара к категории 18+ 
    /// (true - товар относится к категории 18+, в противном случае false);
    /// </summary>
    public bool? adult;

    /// <summary>
    /// Возрастная категория товара.
    /// </summary>
    public yml_age age;

    // NOTE: Не найдено в описании от 23 мая 2018 г.
    // https://yandex.ru/support/market-tech-requirements/index.html
    // public string aliases;

    /// <summary>
    /// Исполнитель.
    /// </summary>
    public string artist;

    /// <summary>
    /// Автор произведения.
    /// </summary>
    public string author;

    /// <summary>
    /// Штрихкод товара от производителя в одном из форматов: EAN-13, EAN-8, UPC-A, UPC-E.
    /// </summary>
    [XmlElement]
    public string[] barcode;

    /// <summary>
    /// Формат.
    /// </summary>
    public string binding;

    // NOTE: Не найдено в описании от 23 мая 2018 г.
    // https://yandex.ru/support/market-tech-requirements/index.html
    // public string buyurl;

    /// <summary>
    /// Идентификатор категории товара, присвоенный магазином (целое число, не более 18 знаков).
    /// 
    /// Обязательный элемент.
    /// </summary>
    public yml_categoryId categoryId;

    /// <summary>
    /// Страна исполнителя.
    /// </summary>
    public string country;

    /// <summary>
    /// Страна производства товара.
    /// </summary>
    public string country_of_origin;

    // NOTE: Программа «Заказ на Маркете» закрыта.
    /// <summary>
    /// Признак участия товарного предложение в проекте «Покупка на Маркете»
    /// </summary>
    // public string cpa;

    /// <summary>
    /// Валюта, в которой указана цена товара: RUR, USD, EUR, UAH, KZT, BYN.
    /// 
    /// Обязательный элемент.
    /// </summary>
    public string currencyId;

    /// <summary>
    /// Даты заездов. Предпочтительный формат: YYYY-MM-DD hh:mm:ss.
    /// </summary>
    [XmlElement]
    public string[] dataTour;

    /// <summary>
    /// Дата и время сеанса. Предпочтительный формат: YYYY-MM-DD hh:mm:ss.
    /// </summary>
    public string date;

    /// <summary>
    /// Количество дней тура.
    /// </summary>
    public string days;

    /// <summary>
    /// Доставка.
    /// Возможность курьерской доставки товара по адресу покупателя в регионе магазина.
    /// Возможные значения:
    ///     true — магазин доставляет товар по адресу покупателя;
    ///     false— магазин не доставляет товар по адресу покупателя.
    /// </summary>
    [DefaultValue(true)]
    public bool? delivery;

    /// <summary>
    /// Cтоимость и сроки курьерской доставки по вашему региону. 
    /// </summary>
    [XmlArray("delivery-options")]
    [XmlArrayItem("option", IsNullable = false)]
    public yml_deliveryOption[] deliveryOptions;

    public bool deliveryIncluded;

    /// <summary>
    /// Подробное описание товара (например, его особенности, преимущества и назначение).
    /// </summary>
    public string description;

    /// <summary>
    /// Габариты товара (длина, ширина, высота) в упаковке. Размеры укажите в сантиметрах.
    /// </summary>
    public string dimensions;

    /// <summary>
    /// Режиссер.
    /// </summary>
    public string director;

    /// <summary>
    /// Продукт можно скачать. Если указано true, предложение показывается во всех регионах.
    /// </summary>
    public bool? downloadable;

    /// <summary>
    /// Срок годности / срок службы либо дата истечения срока годности / срока службы.
    /// </summary>
    public string expiry;

    // TODO: check this
    //public string fee;

    /// <summary>
    /// Формат аудиокниги.
    /// </summary>
    public string format;

    /// <summary>
    /// Зал.
    /// </summary>
    public string hall;

    /// <summary>
    /// Ряд и место в зале.
    /// </summary>
    public string hall_part;

    /// <summary>
    /// Звезды отеля.
    /// </summary>
    public string hotel_stars;

    /// <summary>
    /// Что включено в стоимость тура.
    /// </summary>
    public string included;

    /// <summary>
    /// Признак детского мероприятия (true / false).
    /// </summary>
    public bool? is_kids;

    /// <summary>
    /// Признак премьерности мероприятия (true / false).
    /// </summary>
    public bool? is_premiere;

    /// <summary>
    /// Язык, на котором издано произведение.
    /// </summary>
    public string language;

    // NOTE: Используется только в формате CSV.
    /// <summary>
    /// Элемент local_delivery_cost является устаревшим. 
    /// Стоимость курьерской доставки товара по региону магазина.
    /// </summary>
    // public string local_delivery_cost;

    // NOTE: Используется только в формате CSV.
    ///// <summary>
    ///// Срок курьерской доставки товара по региону магазина (в днях).
    ///// </summary>
    //public string local_delivery_days;

    /// <summary>
    /// Официальная гарантия производителя.
    /// </summary>
    public bool? manufacturer_warranty;

    // NOTE: Не найдено в описании от 23 мая 2018 г.
    // https://yandex.ru/support/market-tech-requirements/index.html
    /// <summary>
    /// Категория товара в Яндекс Маркете;
    /// </summary>
    // public yml_market_category market_category;

    /// <summary>
    /// Тип питания (All, HB и т. п.).
    /// </summary>
    public string meal;

    /// <summary>
    /// Носитель.
    /// </summary>
    public string media;

    /// <summary>
    /// Модель товара и важные параметры.
    /// </summary>
    public string model;

    /// <summary>
    /// Тип или категория товара
    /// Производитель или бренд
    /// Модель товара и важные параметры
    /// </summary>
    public string name;

    /// <summary>
    /// Старая цена товара.
    /// </summary>
    public string oldprice;

    /// <summary>
    /// Опции.
    /// </summary>
    public string options;

    // NOTE: Не найдено в описании от 23 мая 2018 г.
    // https://yandex.ru/support/market-tech-requirements/index.html
    // public yml_orderingTime orderingTime;

    /// <summary>
    /// Оригинальное название.
    /// </summary>
    public string originalName;

    /// <summary>
    /// Количество страниц в книге, должно быть целым положительным числом.
    /// </summary>
    public string page_extent;

    /// <summary>
    /// Все важные характеристики товара — цвет, размер, объем, материал, вес, возраст, пол, и т. д.
    /// </summary>
    [XmlElement]
    public yml_param[] param;

    /// <summary>
    /// Номер тома, если издание состоит из нескольких томов.
    /// </summary>
    public string part;

    /// <summary>
    /// Тип аудиокниги (радиоспектакль, «произведение начитано» и т. п.).
    /// </summary>
    public string performance_type;

    /// <summary>
    /// Исполнитель. Если их несколько, перечисляются через запятую.
    /// </summary>
    public string performed_by;

    /// <summary>
    /// Самовывоз
    /// Используйте элемент pickup, чтобы указать возможность получения товара
    /// в пунктах выдачи.Возможные значения:
    ///     true — товар можно получить в одном из пунктов выдачи магазин;
    ///     false — товар нельзя получить в пунктах выдачи.
    /// </summary>
    public bool? pickup;

    /// <summary>
    /// Ссылки на изображения товара.
    /// </summary>
    [XmlElement]
    public string[] picture;

    /// <summary>
    /// Место проведения.
    /// </summary>
    public string place;

    /// <summary>
    /// Актуальная цена товара.
    /// </summary>
    public yml_price price;

    /// <summary>
    /// Максимальная цена тура.
    /// </summary>
    public string price_max;

    /// <summary>
    /// Минимальная цена тура.
    /// </summary>
    public string price_min;

    /// <summary>
    /// Пользовательские параметры для PriceLabs
    /// </summary>
    [XmlElement]
    public yml_pricelabs_param[] pricelabs_param;

    // NOTE: Не найдено в описании от 23 мая 2018 г.
    // https://yandex.ru/support/market-tech-requirements/index.html
    // public string promo;

    // NOTE: Не найдено в описании от 23 мая 2018 г.
    // https://yandex.ru/support/market-tech-requirements/index.html
    // public string provider;

    /// <summary>
    /// Издательство.
    /// </summary>
    public string publisher;

    /// <summary>
    /// Наценка для PriceLabs
    /// Фильтрует предложения по размеру наценки.
    /// Чтобы фильтр работал, вы должны указывать закупочную цену в прайс-листе
    /// </summary>
    public string purchase_price;

    // NOTE: Программа «Заказ на Маркете» закрыта.
    /// <summary>
    /// Элемент предназначен для передачи рекомендованных товаров.
    /// </summary>
    // public string rec;

    /// <summary>
    /// Время звучания, задается в формате mm.ss (минуты.секунды).
    /// </summary>
    public string recording_length;

    /// <summary>
    /// Курорт или город.
    /// </summary>
    public string region;

    // NOTE: Не найдено в описании от 23 мая 2018 г.
    // https://yandex.ru/support/market-tech-requirements/index.html
    // public string related_offer;

    /// <summary>
    /// Тип комнаты (SNG, DBL и т. п.).
    /// </summary>
    public string room;

    /// <summary>
    /// Условия по минимальной сумме заказа, необходимости предоплаты и минимальному количеству товара
    /// в заказе
    /// </summary>
    public string sales_notes;

    // NOTE: Не найдено в описании от 23 мая 2018 г.
    // https://yandex.ru/support/market-tech-requirements/index.html
    // public string seller_warranty;

    /// <summary>
    /// Серия.
    /// </summary>
    public string series;

    /// <summary>
    /// Актеры.
    /// </summary>
    public string starring;

    /// <summary>
    /// Носитель аудиокниги.
    /// </summary>
    public string storage;

    /// <summary>
    /// Покупка без предварительного заказа («на месте»).
    /// Используйте элемент store, чтобы указать возможность покупки товара
    /// без предварительного заказа в точках продажи(торговых залах с витринами). Возможные значения:
    ///     true — товар можно купить «на месте» без предварительного заказа;
    ///     false — товар не продается без предварительного заказа.
    /// </summary>
    public bool? store;

    /// <summary>
    /// Оглавление.
    /// </summary>
    public string table_of_contents;

    // NOTE: Не найдено в описании от 23 мая 2018 г.
    // https://yandex.ru/support/market-tech-requirements/index.html
    // public string tarifplan;

    /// <summary>
    /// Название.
    /// </summary>
    public string title;

    /// <summary>
    /// Транспорт.
    /// </summary>
    public string transport;

    /// <summary>
    /// Тип/ атегория товара (например, «мобильный телефон», «стиральная машина», «угловой диван»).
    /// </summary>
    public string typePrefix;

    /// <summary>
    /// URL страницы товара на сайте магазина. Максимальная длина ссылки — 512 символов.
    /// Допускаются кириллические ссылки.
    /// </summary>
    public string url;

    /// <summary>
    /// Ставка НДС для товара.
    /// </summary>
    public string vat;

    /// <summary>
    /// Производитель или бренд.
    /// </summary>
    public string vendor;

    /// <summary>
    /// Код производителя для данного товара.
    /// </summary>
    public string vendorCode;

    /// <summary>
    /// Общее количество томов, если издание состоит из нескольких томов.
    /// </summary>
    public string volume;

    /// <summary>
    /// Вес товара в килограммах с учетом упаковки.
    /// </summary>
    public string weight;

    /// <summary>
    /// Часть света.
    /// </summary>
    public string worldRegion;

    // NOTE: Не найдено в описании от 23 мая 2018 г.
    // https://yandex.ru/support/market-tech-requirements/index.html
    // public string wprice;

    // NOTE: Не найдено в описании от 23 мая 2018 г.
    // https://yandex.ru/support/market-tech-requirements/index.html
    // public string xCategory;

    /// <summary>
    /// Год издания.
    /// </summary>
    public string year;

    /// <summary>
    /// Идентификатор предложения.
    /// </summary>
    [XmlAttribute]
    public string id;

    /// <summary>
    /// Для всех предложений, которые необходимо отнести к одной модели,
    /// должно быть указано одинаковое значение атрибута group_id.
    /// </summary>
    [XmlAttribute]
    public string group_id;

    /// <summary>
    /// Тип предложения.
    /// </summary>
    [XmlIgnore]
    public yml_offerType? type;

    [XmlAttribute("type")]
    public string TypeSerializable
    {
        get => type?.ToString();
        set
        {
            if (int.TryParse(value, out var i))
            {
                type = (yml_offerType?)i;
            }
        }
    }

    public bool ShouldSerializeTypeSerializable()
    {
        return type.HasValue;
    }

    /// <remarks/>
    [XmlIgnore]
    public bool typeSpecified;

    /// <summary>
    /// Статус товара — «готов к отправке» или «на заказ». 
    /// </summary>
    [XmlAttribute]
    public bool available;

    /// <remarks/>
    [XmlIgnore]
    public bool availableSpecified;

    /// <summary>
    /// Размер ставки на остальных местах размещения (все, кроме карточки товара).
    /// </summary>
    [XmlAttribute]
    public string bid;

    /// <summary>
    /// Размер ставки на карточке товара.
    /// </summary>
    [XmlAttribute]
    public string cbid;

    // NOTE: Не найдено в описании от 23 мая 2018 г.
    // https://yandex.ru/support/market-tech-requirements/index.html
    // [System.Xml.Serialization.XmlAttributeAttribute()]
    // public string sbid;

    /// <remarks/>
    [XmlAttribute]
    public string fee;
}

/// <summary>
/// Возрастная категория товара.
/// </summary>
[GeneratedCode("xsd", "4.7.3062.0")]
[Serializable]
[DebuggerStepThrough]
[DesignerCategory("code")]
[XmlType(AnonymousType = true)]
[XmlRoot(Namespace = "", IsNullable = false)]
public class yml_age
{

    /// <summary>
    /// Годы(year) или месяцы(month)
    /// </summary>
    [XmlAttribute]
    public string unit;

    /// <summary>
    /// Значение параметра.
    /// </summary>
    [XmlText]
    public string Value;
}

/// <remarks/>
[GeneratedCode("xsd", "4.7.3062.0")]
[Serializable]
[DebuggerStepThrough]
[DesignerCategory("code")]
[XmlType(AnonymousType = true)]
[XmlRoot(Namespace = "", IsNullable = false)]
public class yml_categoryId
{

    /// <remarks/>
    [XmlAttribute]
    [DefaultValue(yml_categoryIdType.Own)]
    public yml_categoryIdType type;

    /// <summary>
    /// Название категории товара.
    /// 
    /// Обязательный параметр
    /// </summary>
    [XmlText]
    public string Value;

    public yml_categoryId()
    {
        type = yml_categoryIdType.Own;
    }
}

/// <remarks/>
[GeneratedCode("xsd", "4.7.3062.0")]
[Serializable]
[XmlType(AnonymousType = true)]
public enum yml_categoryIdType
{

    /// <remarks/>
    Yandex,

    /// <remarks/>
    Torg,

    /// <remarks/>
    Own
}

/// <remarks/>
[GeneratedCode("xsd", "4.7.3062.0")]
[Serializable]
[DebuggerStepThrough]
[DesignerCategory("code")]
[XmlType(AnonymousType = true)]
[XmlRoot(Namespace = "", IsNullable = false)]
public class yml_market_category
{

    /// <remarks/>
    [XmlAttribute]
    public string id;

    /// <remarks/>
    [XmlText]
    public string Value;
}

/// <remarks/>
[GeneratedCode("xsd", "4.7.3062.0")]
[Serializable]
[DebuggerStepThrough]
[DesignerCategory("code")]
[XmlType(AnonymousType = true)]
[XmlRoot(Namespace = "", IsNullable = false)]
public class yml_orderingTime
{

    /// <remarks/>
    //public onstock onstock;
    public string onstock;

    /// <remarks/>
    public yml_ordering ordering;

    /// <remarks/>
    public string deliveryTime;
}

/// <remarks/>
[GeneratedCode("xsd", "4.7.3062.0")]
[Serializable]
[DebuggerStepThrough]
[DesignerCategory("code")]
[XmlType(AnonymousType = true)]
[XmlRoot(Namespace = "", IsNullable = false)]
public class yml_ordering
{

    /// <remarks/>
    [XmlAttribute]
    public string hours;

    /// <remarks/>
    [XmlText]
    public string Value;
}

/// <remarks/>
[GeneratedCode("xsd", "4.7.3062.0")]
[Serializable]
[DebuggerStepThrough]
[DesignerCategory("code")]
[XmlType(AnonymousType = true)]
[XmlRoot(Namespace = "", IsNullable = false)]
public class yml_param
{

    /// <summary>
    /// Название параметра.
    /// 
    /// Обязательный парметер.
    /// </summary>
    [XmlAttribute]
    public string name;

    /// <summary>
    /// Единицы измерения (для числовых параметров, опционально).
    /// </summary>
    [XmlAttribute]
    public string unit;

    /// <summary>
    /// Значение параметра
    /// </summary>
    [XmlText]
    public string Value;
}

/// <remarks/>
[GeneratedCode("xsd", "4.7.3062.0")]
[Serializable]
[DebuggerStepThrough]
[DesignerCategory("code")]
[XmlType(AnonymousType = true)]
[XmlRoot(Namespace = "", IsNullable = false)]
public class yml_price
{

    /// <summary>
    /// Начальная цена «от» (true).
    /// </summary>
    [XmlAttribute]
    public bool from;

    /// <summary>
    /// Цена.
    /// </summary>
    [XmlText]
    public string Value;
}

/// <summary>
/// Пользовательские параметры для PriceLabs
/// 
/// Пример:
///     Параметр для фильтрации по остаткам на складе:
///     <pricelabs_param namе="Остаток на складе">15</pricelabs_param >
/// </summary>
[GeneratedCode("xsd", "4.7.3062.0")]
[Serializable]
[DebuggerStepThrough]
[DesignerCategory("code")]
[XmlType(AnonymousType = true)]
[XmlRoot(Namespace = "", IsNullable = false)]
public class yml_pricelabs_param
{

    /// <summary>
    /// Имя параметра.
    /// </summary>
    [XmlAttribute]
    public string name;

    /// <summary>
    /// Значение параметра
    /// </summary>
    [XmlText]
    public string Value;
}


/// <summary>
/// Тип описания предложения.
/// 
/// Для большинства категорий товаров подходят следующие типы описаний:
///     Упрощенный тип описания — элемент type не указывается.
///     Произвольный тип описания — type имеет значение vendor.model.
/// 
/// Для некоторых категорий товаров нужно использовать собственные типы описаний
/// </summary>
[GeneratedCode("xsd", "4.7.3062.0")]
[Serializable]
[XmlType(AnonymousType = true)]
public enum yml_offerType
{
    /// <summary>
    /// Произвольный тип описания.
    /// </summary>
    [XmlEnum("vendor.model")]
    vendormodel,

    /// <summary>
    /// Лекарства.
    /// </summary>
    medicine,

    /// <summary>
    /// Книги.
    /// </summary>
    book,

    /// <summary>
    /// Аудиокниги.
    /// </summary>
    audiobook,

    /// <summary>
    /// Музыкальная и видеопродукция.
    /// </summary>
    [XmlEnum("artist.title")]
    artisttitle,

    /// <summary>
    /// Туры.
    /// </summary>
    tour,

    // NOTE: Не найдено в описании от 23 мая 2018 г.
    // https://yandex.ru/support/market-tech-requirements/index.html
    ///// <summary>
    ///// The ticket
    ///// </summary>
    //ticket,

    /// <summary>
    /// Билеты на мероприятия.
    /// </summary>
    [XmlEnum("event-ticket")]
    eventticket
}


/// <summary>
/// Описание подарка.
/// </summary>
[Serializable]
[DebuggerStepThrough]
[DesignerCategory("code")]
[XmlType(AnonymousType = true)]
[XmlRoot(Namespace = "", IsNullable = false)]
public class yml_gift
{
    /// <summary>
    /// Идентификатор товара.
    /// </summary>
    [XmlAttribute]
    public string id;

    /// <summary>
    /// Название товара.
    /// </summary>
    public string name;

    /// <summary>
    /// Ссылка на изображение.
    /// </summary>
    public string picture;
}


/// <summary>
/// Описание акции.
/// </summary>
[Serializable]
[DebuggerStepThrough]
[DesignerCategory("code")]
[XmlType(AnonymousType = true)]
[XmlRoot(Namespace = "", IsNullable = false)]
public class yml_promo
{
    /// <summary>
    /// Идентификатор акции.
    /// </summary>
    [XmlAttribute]
    public string id;

    /// <summary>
    /// Тип акции.
    /// </summary>
    [XmlAttribute]
    public string type;

    /// <summary>
    /// Краткое описание акции. 
    /// </summary>
    public string description;

    /// <summary>
    /// Ссылка на описание акции на сайте магазина.
    /// </summary>
    public string url;

    /// <summary>
    /// Текст промокода. Максимальная длина — 20 символов.
    /// </summary>
    [XmlElement("promo-code")]
    public string promoCode;

    /// <summary>
    /// Размер скидки в процентах от стоимости или в валюте.
    /// </summary>
    public yml_discount discount;

    /// <summary>
    /// Дата и время начала акции. Допустимые форматы: YYYY-MM-DD hh:mm:ss или YYYY-MM-DD.
    /// </summary>
    [XmlElement("start-date")]
    public string startDate;

    /// <summary>
    /// Дата и время завершения акции. Допустимые форматы: YYYY-MM-DD hh:mm:ss или YYYY-MM-DD.
    /// </summary>
    [XmlElement("end-date")]
    public string endDate;

    /// <summary>
    /// Информация о товарах, участвующих в акции.
    /// </summary>
    public yml_purchase purchase;

    /// <summary>
    /// Подарки, участвующие в акции. Максимальное число подарков на выбор — 12.
    /// </summary>
    [XmlArray("promo-gifts")]
    [XmlArrayItem("promo-gift", IsNullable = false)]
    public yml_promoGift[] promoGifts;
}


/// <summary>
/// Информация о товарах, участвующих в акции.
/// </summary>
[Serializable]
[DebuggerStepThrough]
[DesignerCategory("code")]
[XmlType(AnonymousType = true)]
[XmlRoot(Namespace = "", IsNullable = false)]
public class yml_purchase
{
    /// <summary>
    /// Количество товаров, которое нужно приобрести, чтобы получить подарок.
    /// </summary>
    [XmlElement("required-quantity")]
    public string requiredQuantity;

    /// <summary>
    /// Количество товаров, которые покупатель получит в подарок.
    /// </summary>
    [XmlElement("free-quantity")]
    public string freeQuantity;

    /// <summary>
    /// Товары и/или категории, на которые действует акция
    /// </summary>
    //[XmlArrayItem("product", IsNullable = false)]
    [XmlElement]
    public yml_product[] product;
}

/// <summary>
/// Товар и/или категория, на который(-ою) действует акция
/// </summary>
[Serializable]
[DebuggerStepThrough]
[DesignerCategory("code")]
[XmlType(AnonymousType = true)]
[XmlRoot(Namespace = "", IsNullable = false)]
public class yml_product
{
    /// <summary>
    /// Идентификатор предложения.
    /// </summary>
    [XmlAttribute("offer-id")]
    public string offerId;

    /// <summary>
    /// Идентификатор категории.
    /// </summary>
    [XmlAttribute("category-id")]
    public string categoryId;

    /// <summary>
    /// Цена на время акции.
    /// </summary>
    [XmlElement("discount-price")]
    public yml_discountPrice discountPrice;

}

/// <summary>
/// Размер скидки в процентах от стоимости или в валюте. Тип скидки указывается в атрибутах:
///     unit="percent" — скидка в процентах, доступные значения: от 5 до 95 процентов;
///     unit="currency" currency="RUR" — скидка в валюте, доступные значения: кратные 100.
/// </summary>
[Serializable]
[DebuggerStepThrough]
[DesignerCategory("code")]
[XmlType(AnonymousType = true)]
[XmlRoot(Namespace = "", IsNullable = false)]
public class yml_discount
{
    /// <summary>
    /// Тип скидки(в процентах или валюте).
    /// </summary>
    [XmlAttribute]
    public string unit;

    /// <summary>
    /// Валюта.
    /// </summary>
    [XmlAttribute]
    public string currency;

    /// <summary>
    /// Размер скидки.
    /// </summary>
    [XmlText]
    public string Value;
}


/// <summary>
/// Цена на время акции.
/// </summary>
[Serializable]
[DebuggerStepThrough]
[DesignerCategory("code")]
[XmlType(AnonymousType = true)]
[XmlRoot(Namespace = "", IsNullable = false)]
public class yml_discountPrice
{
    /// <summary>
    /// Валюта.
    /// </summary>
    [XmlAttribute]
    public string currency;

    /// <summary>
    /// Новая цена.
    /// </summary>
    [XmlText]
    public string Value;
}


/// <summary>
/// Подарок, участвующий в акции.
/// </summary>
[Serializable]
[DebuggerStepThrough]
[DesignerCategory("code")]
[XmlType(AnonymousType = true)]
[XmlRoot(Namespace = "", IsNullable = false)]
public class yml_promoGift
{
    /// <summary>
    /// Идентификатор подарка, который есть в offers.
    /// </summary>
    [XmlAttribute("offer-id")]
    public string offerId;

    /// <summary>
    /// Идентификатор подарка, который есть в gifts.
    /// </summary>
    [XmlAttribute("gift-id")]
    public string giftId;
}