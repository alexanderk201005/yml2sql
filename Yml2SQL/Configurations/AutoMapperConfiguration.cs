﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Yml2SQL.Helpers;
using YmlModel.Model;

namespace Yml2SQL.Configurations
{
    public static class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<yml_age, YmlAge>();
                cfg.CreateMap<string, YmlOfferBarcode>()
                    .ForMember(dest => dest.Value,
                        opts => opts.MapFrom(src => src))
                    .Ignore(property => property.YmlOfferKey);
                cfg.CreateMap<yml_catalog, YmlCatalog>();
                cfg.CreateMap<yml_category, YmlCategory>();
                cfg.CreateMap<yml_categoryId, YmlCategoryId>();
                cfg.CreateMap<yml_currency, YmlCurrency>();
                cfg.CreateMap<string, YmlOfferDataTour>()
                    .ForMember(dest => dest.Value,
                        opts => opts.MapFrom(src => src))
                    .Ignore(property => property.YmlOfferKey);
                cfg.CreateMap<yml_deliveryOption, YmlShopDeliveryOption>()
                    .Ignore(property => property.YmlShopKey);
                cfg.CreateMap<yml_deliveryOption, YmlOfferDeliveryOption>()
                    .Ignore(property => property.YmlOfferKey);
                cfg.CreateMap<yml_discount, YmlDiscount>();
                cfg.CreateMap<yml_discountPrice, YmlDiscountPrice>();
                cfg.CreateMap<yml_gift, YmlGift>();
                cfg.CreateMap<yml_offer, YmlOffer>()
                    .ForMember(dest => dest.Barcodes,
                        opts => opts.MapFrom(src => src.barcode))
                    .ForMember(dest => dest.DeliveryOptions,
                            opts => opts.MapFrom(src => src.deliveryOptions))
                    .ForMember(dest => dest.DataTours,
                        opts => opts.MapFrom(src => src.dataTour))
                    .ForMember(dest => dest.Pictures,
                        opts => opts.MapFrom(src => src.picture));
                cfg.CreateMap<yml_param, YmlParam>()
                    .Ignore(property => property.YmlOfferKey);
                cfg.CreateMap<string, YmlOfferPicture>()
                    .ForMember(dest=> dest.Url,
                        opts => opts.MapFrom(src => src))
                    .Ignore(property => property.YmlOfferKey);
                cfg.CreateMap<yml_price, YmlPrice>();
                cfg.CreateMap<yml_pricelabs_param, YmlPriceLabsParam>()
                    .Ignore(property => property.YmlOfferKey);
                cfg.CreateMap<yml_promo, YmlPromo>()
                    .ForMember(dest => dest.PromoGifts,
                    opts => opts.MapFrom(src => src.promoGifts));
                cfg.CreateMap<yml_promoGift, YmlPromoGift>()
                    .Ignore(property => property.YmlPromoKey);
                cfg.CreateMap<yml_product, YmlProduct>()
                    .Ignore(property => property.YmlPurchaseKey);
                cfg.CreateMap<yml_purchase, YmlPurchase>()
                    .ForMember(dest => dest.Products,
                        opts => opts.MapFrom(src => src.product));
                cfg.CreateMap<yml_shop, YmlShop>();
            });
        }
    }
}
