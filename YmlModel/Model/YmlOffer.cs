﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YmlModel.Model
{
    /// <summary>
    /// Тип описания предложения.
    /// </summary>
    public enum YmlOfferType
    {
        /// <summary>
        /// Произвольный тип описания.
        /// </summary>
        VendorModel,

        /// <summary>
        /// Лекарства.
        /// </summary>
        Medicine,

        /// <summary>
        /// Книги.
        /// </summary>
        Book,

        /// <summary>
        /// Аудиокниги.
        /// </summary>
        Audiobook,

        /// <summary>
        /// Музыкальная и видеопродукция.
        /// </summary>
        Artisttitle,

        /// <summary>
        /// Туры.
        /// </summary>
        Tour,

        /// <summary>
        /// Билеты на мероприятия.
        /// </summary>
        EventTicket
    }

    /// <summary>
    /// Предложение магазина.ы
    /// </summary>
    public class YmlOffer
    {
        [Key]
        public long Key { get; set; }

        /// <summary>
        /// International Standard Book Number — международный уникальный номер книжного издания.
        /// Если их несколько, указываются через запятую.
        /// </summary>
        public string ISBN { get; set; }


        /// <summary>
        /// Отношение товара к категории 18+ 
        /// (true - товар относится к категории 18+, в противном случае false);
        /// </summary>
        public bool? Adult { get; set; }

        /// <summary>
        /// Возрастная категория товара.
        /// </summary>
        public virtual YmlAge Age { get; set; }
        
        /// <summary>
        /// Исполнитель.
        /// </summary>
        public string Artist { get; set; }

        /// <summary>
        /// Автор произведения.
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// Штрихкод товара от производителя в одном из форматов: EAN-13, EAN-8, UPC-A, UPC-E.
        /// </summary>
        public virtual ICollection<YmlOfferBarcode> Barcodes { get; set; }

        /// <summary>
        /// Формат.
        /// </summary>
        public string Binding { get; set; }

        /// <summary>
        /// Идентификатор категории товара, присвоенный магазином (целое число, не более 18 знаков).
        /// 
        /// Обязательный элемент.
        /// </summary>
        public virtual YmlCategoryId CategoryId { get; set; }

        /// <summary>
        /// Страна исполнителя.
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Страна производства товара.
        /// </summary>
        public string CountryOfOrigin { get; set; }

        /// <summary>
        /// Валюта, в которой указана цена товара: RUR, USD, EUR, UAH, KZT, BYN.
        /// 
        /// Обязательный элемент.
        /// </summary>
        public string CurrencyId { get; set; }

        /// <summary>
        /// Даты заездов. Предпочтительный формат: YYYY-MM-DD hh:mm:ss.
        /// </summary>
        public virtual ICollection<YmlOfferDataTour> DataTours { get; set; }

        /// <summary>
        /// Дата и время сеанса. Предпочтительный формат: YYYY-MM-DD hh:mm:ss.
        /// </summary>
        public string Date { get; set; }

        /// <summary>
        /// Количество дней тура.
        /// </summary>
        public string Days { get; set; }

        /// <summary>
        /// Доставка.
        /// Возможность курьерской доставки товара по адресу покупателя в регионе магазина.
        /// Возможные значения:
        ///     true — магазин доставляет товар по адресу покупателя;
        ///     false— магазин не доставляет товар по адресу покупателя.
        /// </summary>
        public bool? Delivery { get; set; }

        /// <summary>
        /// Cтоимость и сроки курьерской доставки по вашему региону. 
        /// </summary>
        public virtual ICollection<YmlOfferDeliveryOption> DeliveryOptions { get; set; }

        /// <summary>
        /// Подробное описание товара (например, его особенности, преимущества и назначение).
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Габариты товара (длина, ширина, высота) в упаковке. Размеры укажите в сантиметрах.
        /// </summary>
        public string Dimensions { get; set; }

        /// <summary>
        /// Режиссер.
        /// </summary>
        public string Director { get; set; }

        /// <summary>
        /// Продукт можно скачать. Если указано true, предложение показывается во всех регионах.
        /// </summary>
        public bool? Downloadable { get; set; }

        /// <summary>
        /// Срок годности / срок службы либо дата истечения срока годности / срока службы.
        /// </summary>
        public string Expiry { get; set; }
        
        /// <summary>
        /// Формат аудиокниги.
        /// </summary>
        public string Format { get; set; }

        /// <summary>
        /// Зал.
        /// </summary>
        public string Hall { get; set; }

        /// <summary>
        /// Ряд и место в зале.
        /// </summary>
        public string HallPart { get; set; }

        /// <summary>
        /// Звезды отеля.
        /// </summary>
        public string HotelStars { get; set; }

        /// <summary>
        /// Что включено в стоимость тура.
        /// </summary>
        public string Included { get; set; }

        /// <summary>
        /// Признак детского мероприятия (true / false).
        /// </summary>
        public bool? IsKids { get; set; }

        /// <summary>
        /// Признак премьерности мероприятия (true / false).
        /// </summary>
        public bool? IsPremiere { get; set; }

        /// <summary>
        /// Язык, на котором издано произведение.
        /// </summary>
        public string Language { get; set; }

        /// <summary>
        /// Официальная гарантия производителя.
        /// </summary>
        public bool? ManufacturerWarranty { get; set; }

        /// <summary>
        /// Тип питания (All, HB и т. п.).
        /// </summary>
        public string Meal { get; set; }

        /// <summary>
        /// Носитель.
        /// </summary>
        public string Media { get; set; }

        /// <summary>
        /// Модель товара и важные параметры.
        /// </summary>
        public string Model { get; set; }

        /// <summary>
        /// Тип или категория товара
        /// Производитель или бренд
        /// Модель товара и важные параметры
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Старая цена товара.
        /// </summary>
        public string OldPrice { get; set; }

        /// <summary>
        /// Опции.
        /// </summary>
        public string Options { get; set; }

        /// <summary>
        /// Оригинальное название.
        /// </summary>
        public string OriginalName { get; set; }

        /// <summary>
        /// Количество страниц в книге, должно быть целым положительным числом.
        /// </summary>
        public string PageExtent { get; set; }

        /// <summary>
        /// Все важные характеристики товара — цвет, размер, объем, материал, вес, возраст, пол, и т. д.
        /// </summary>
        public virtual ICollection<YmlParam> Params { get; set; }

        /// <summary>
        /// Номер тома, если издание состоит из нескольких томов.
        /// </summary>
        public string Part { get; set; }

        /// <summary>
        /// Тип аудиокниги (радиоспектакль, «произведение начитано» и т. п.).
        /// </summary>
        public string PerformanceType { get; set; }

        /// <summary>
        /// Исполнитель. Если их несколько, перечисляются через запятую.
        /// </summary>
        public string PerformedBy { get; set; }

        /// <summary>
        /// Самовывоз
        /// Используйте элемент pickup, чтобы указать возможность получения товара
        /// в пунктах выдачи.Возможные значения:
        ///     true — товар можно получить в одном из пунктов выдачи магазин;
        ///     false — товар нельзя получить в пунктах выдачи.
        /// </summary>
        public bool? Pickup { get; set; }

        /// <summary>
        /// Ссылки на изображения товара.
        /// </summary>
        public virtual ICollection<YmlOfferPicture> Pictures { get; set; }

        /// <summary>
        /// Место проведения.
        /// </summary>
        public string Place { get; set; }

        /// <summary>
        /// Актуальная цена товара.
        /// </summary>
        public virtual YmlPrice Price { get; set; }

        /// <summary>
        /// Максимальная цена тура.
        /// </summary>
        public string PriceMax { get; set; }

        /// <summary>
        /// Минимальная цена тура.
        /// </summary>
        public string PriceMin { get; set; }

        /// <summary>
        /// Пользовательские параметры для PriceLabs
        /// </summary>
        public virtual ICollection<YmlPriceLabsParam> PricelabsParams { get; set; }

        /// <summary>
        /// Издательство.
        /// </summary>
        public string Publisher { get; set; }

        /// <summary>
        /// Наценка для PriceLabs
        /// Фильтрует предложения по размеру наценки.
        /// Чтобы фильтр работал, вы должны указывать закупочную цену в прайс-листе
        /// </summary>
        public string PurchasePrice { get; set; }

        /// <summary>
        /// Время звучания, задается в формате mm.ss (минуты.секунды).
        /// </summary>
        public string RecordingLength { get; set; }

        /// <summary>
        /// Курорт или город.
        /// </summary>
        public string Region { get; set; }

        /// <summary>
        /// Тип комнаты (SNG, DBL и т. п.).
        /// </summary>
        public string Room { get; set; }

        /// <summary>
        /// Условия по минимальной сумме заказа, необходимости предоплаты и минимальному количеству товара
        /// в заказе
        /// </summary>
        public string SalesNotes { get; set; }

        /// <summary>
        /// Серия.
        /// </summary>
        public string Series { get; set; }

        /// <summary>
        /// Актеры.
        /// </summary>
        public string Starring { get; set; }

        /// <summary>
        /// Носитель аудиокниги.
        /// </summary>
        public string Storage { get; set; }

        /// <summary>
        /// Покупка без предварительного заказа («на месте»).
        /// Используйте элемент store, чтобы указать возможность покупки товара
        /// без предварительного заказа в точках продажи(торговых залах с витринами). Возможные значения:
        ///     true — товар можно купить «на месте» без предварительного заказа;
        ///     false — товар не продается без предварительного заказа.
        /// </summary>
        public bool? Store { get; set; }

        /// <summary>
        /// Оглавление.
        /// </summary>
        public string TableOfContents { get; set; }

        /// <summary>
        /// Название.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Транспорт.
        /// </summary>
        public string Transport { get; set; }

        /// <summary>
        /// Тип/ атегория товара (например, «мобильный телефон», «стиральная машина», «угловой диван»).
        /// </summary>
        public string TypePrefix { get; set; }

        /// <summary>
        /// URL страницы товара на сайте магазина. Максимальная длина ссылки — 512 символов.
        /// Допускаются кириллические ссылки.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Ставка НДС для товара.
        /// </summary>
        public string Vat { get; set; }

        /// <summary>
        /// Производитель или бренд.
        /// </summary>
        public string Vendor { get; set; }

        /// <summary>
        /// Код производителя для данного товара.
        /// </summary>
        public string VendorCode { get; set; }

        /// <summary>
        /// Общее количество томов, если издание состоит из нескольких томов.
        /// </summary>
        public string Volume { get; set; }

        /// <summary>
        /// Вес товара в килограммах с учетом упаковки.
        /// </summary>
        public string Weight { get; set; }

        /// <summary>
        /// Часть света.
        /// </summary>
        public string WorldRegion { get; set; }

        /// <summary>
        /// Год издания.
        /// </summary>
        public string Year { get; set; }

        /// <summary>
        /// Идентификатор предложения.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Для всех предложений, которые необходимо отнести к одной модели,
        /// должно быть указано одинаковое значение атрибута group_id.
        /// </summary>
        public string GroupId { get; set; }

        /// <summary>
        /// Тип описания предложения.
        /// </summary>
        public YmlOfferType? Type { get; set; }

        /// <summary>
        /// Статус товара — «готов к отправке» или «на заказ». 
        /// </summary>
        public bool? Available { get; set; }
        
        /// <summary>
        /// Размер ставки на остальных местах размещения (все, кроме карточки товара).
        /// </summary>
        public string Bid { get; set; }

        /// <summary>
        /// Размер ставки на карточке товара.
        /// </summary>
        public string Cbid { get; set; }

        /// <remarks/>
        public string Fee { get; set; }

        public long YmlShopKey { get; set; }

        public YmlOffer()
        {
            Barcodes = new HashSet<YmlOfferBarcode>();
            DataTours = new HashSet<YmlOfferDataTour>();
            DeliveryOptions = new HashSet<YmlOfferDeliveryOption>();
            Params = new HashSet<YmlParam>();
            Pictures = new HashSet<YmlOfferPicture>();
            PricelabsParams = new HashSet<YmlPriceLabsParam>();
        }
    }
}
