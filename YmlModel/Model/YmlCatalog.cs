﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YmlModel.Model
{
    /// <summary>
    /// Корневой элемент XML-документа.
    /// </summary>
    public class YmlCatalog
    {
        [Key]
        public long Key { get; set; }

        /// <summary>
        ///  Элемент содержит описание магазина (Shop) и его предложений (Offers):
        /// </summary>
        public virtual YmlShop Shop { get; set; }

        /// <summary>
        /// Дате и время генерации XML-файла на стороне магазина.
        /// Дата должна иметь формат YYYY-MM-DD HH:mm.
        /// </summary>
        [Required]
        public string Date { get; set; }
    }
}
