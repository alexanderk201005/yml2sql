﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YmlModel.Model
{
    /// <summary>
    /// Идентификатор валюты товара.
    /// </summary>
    public enum CurrencyId
    {
        RUR,

        RUB,

        USD,

        BYR,

        BYN,

        KZT,

        EUR,

        UAH,
    }

    /// <summary>
    /// Курс валюты.
    /// </summary>
    public class YmlCurrency
    {
        [Key]
        public long Key { get; set; }

        /// <summary>
        /// Код одной или нескольких валют.
        /// </summary>
        public CurrencyId Id { get; set; }

        /// <summary>
        /// Указывает курс валюты к курсу основной валюты,
        /// взятой за единицу (валюта, для которой rate="1"). Параметр rate может иметь следующие значения:
        ///     Постоянное число — внутренний курс, который вы используете.
        ///     CBRF — курс по Центральному банку РФ.
        ///     NBU — курс по Национальному банку Украины.
        ///     NBK — курс по Национальному банку Казахстана.
        ///     СВ — курс по банку той страны, к которой относится магазин по своему региону,
        ///          указанному в личном кабинете.
        /// В качестве основной валюты (для которой установлено rate= "1") могут быть использованы
        /// только рубль(RUR, RUB), белорусский рубль(BYN), гривна(UAH) или тенге(KZT).
        /// </summary>
        public string Rate { get; set; }

        /// <summary>
        /// % процент прибавляемый к курсу.
        /// </summary>
        public string Plus { get; set; }

        public long YmlShopKey { get; set; }

        public YmlCurrency()
        {
            Rate = "1";
            Plus = "0";
        }
    }
}
