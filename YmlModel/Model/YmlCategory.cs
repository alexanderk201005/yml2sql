﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YmlModel.Model
{
    /// <summary>
    /// Категория товара.
    /// </summary>
    public class YmlCategory
    {
        [Key]
        public long Key { get; set; }

        /// <summary>
        /// Идентификатор категории.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Идентификатор категории более высокого уровня — ParentId (для подкатегорий).
        /// </summary>
        public string ParentId { get; set; }

        /// <summary>
        /// Название категории.
        /// </summary>
        public string Value { get; set; }

        public long YmlShopKey { get; set; }
    }
}
