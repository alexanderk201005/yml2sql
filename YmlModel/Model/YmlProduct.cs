﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YmlModel.Model
{
    public class YmlProduct
    {
        [Key]
        public long Key { get; set; }

        /// <summary>
        /// Идентификатор предложения.
        /// </summary>
        public string OfferId { get; set; }

        /// <summary>
        /// Идентификатор категории.
        /// </summary>
        public string CategoryId { get; set; }

        /// <summary>
        /// Цена на время акции.
        /// </summary>
        public virtual YmlDiscountPrice DiscountPrice { get; set; }

        public long YmlPurchaseKey { get; set; }
    }
}
