﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YmlModel.Model
{
    /// <summary>
    /// Штрихкод товара/
    /// </summary>
    public class YmlOfferBarcode
    {
        [Key]
        public long Key { get; set; }

        /// <summary>
        /// Значение параметра.
        /// </summary>
        public string Value { get; set; }

        public long YmlOfferKey { get; set; }
    }
}
