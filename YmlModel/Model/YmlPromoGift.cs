﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YmlModel.Model
{
    /// <summary>
    /// Подарок, участвующий в акции.
    /// </summary>
    public class YmlPromoGift
    {
        [Key]
        public long Key { get; set; }

        /// <summary>
        /// Идентификатор подарка, который есть в offers.
        /// </summary>
        public string OfferId { get; set; }

        /// <summary>
        /// Идентификатор подарка, который есть в gifts.
        /// </summary>
        public string GiftId { get; set; }

        public long YmlPromoKey { get; set; }
    }
}
