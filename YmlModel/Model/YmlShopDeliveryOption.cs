﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YmlModel.Model
{
    /// <summary>
    /// Условия курьерской доставки товара.
    /// </summary>
    public class YmlShopDeliveryOption
    {
        [Key]
        public long Key { get; set; }

        /// <summary>
        /// Стоимость доставки.
        /// </summary>
        public string Cost { get; set; }

        /// <summary>
        /// Срок доставки в рабочих днях.
        /// </summary>
        public string Days { get; set; }

        /// <summary>
        /// Время, до которого нужно сделать заказ, чтобы получить его в этот срок.
        /// 
        /// Необязательный элемент.
        /// </summary>
        public string OrderBefore { get; set; }


        public long YmlShopKey { get; set; }
    }
}
