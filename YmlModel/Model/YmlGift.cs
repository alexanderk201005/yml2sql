﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YmlModel.Model
{
    /// <summary>
    /// Описание подарка.
    /// </summary>
    public class YmlGift
    {
        [Key]
        public long Key { get; set; }

        /// <summary>
        /// Идентификатор товара.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Название товара.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Ссылка на изображение.
        /// </summary>
        public string Picture { get; set; }

        public long YmlShopKey { get; set; }
    }
}
