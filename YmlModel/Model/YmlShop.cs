﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YmlModel.Model
{
    /// <summary>
    /// Описание магазина и его товарных предложений.
    /// </summary>
    public class YmlShop
    {
        [Key]
        public long Key { get; set; }
        /// <summary>
        /// Наименование агентства, которое оказывает техническую поддержку магазину
        /// и отвечает за работоспособность сайта.
        /// 
        /// Необязательный элемент.
        /// </summary>
        public string Agency { get; set; }

        /// <summary>
        /// Список категорий магазина.
        /// 
        /// Обязательный элемент.
        /// </summary>
        public virtual ICollection<YmlCategory> Categories { get; set; }

        /// <summary>
        /// Полное наименование компании, владеющей магазином. Не публикуется,
        /// используется для внутренней идентификации.
        /// 
        /// Обязательный элемент.
        /// </summary>
        public string Company { get; set; }

        /// <summary>
        /// Список курсов валют магазина.
        /// 
        /// Обязательный элемент.
        /// </summary>
        public virtual ICollection<YmlCurrency> Currencies { get; set; }

        /// <summary>
        /// Стоимость и сроки курьерской доставки по региону, в котором находится магазин.
        /// 
        /// Обязательный элемент, если все данные по доставке передаются в прайс-листе.
        /// </summary>
        public virtual ICollection<YmlShopDeliveryOption> DeliveryOptions { get; set; }

        /// <summary>
        /// Контактный адрес разработчиков CMS или агентства, осуществляющего техподдержку.
        /// 
        /// Необязательный элемент.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Короткое название магазина, не более 20 символов.
        /// В названии нельзя использовать слова, не имеющие отношения к наименованию магазина,
        /// например «лучший», «дешевый», указывать номер телефона и т. п.
        /// Название магазина должно совпадать с фактическим названием магазина,
        /// которое публикуется на сайте.При несоблюдении этого требования наименование
        /// Яндекс.Маркет может самостоятельно изменить название без уведомления магазина.
        /// 
        /// Обязательный элемент.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Система управления контентом, на основе которой работает магазин (CMS).
        /// 
        /// Необязательный элемент.
        /// </summary>
        public string Platform { get; set; }

        /// <summary>
        /// URL главной страницы магазина. Максимум 50 символов. Допускаются кириллические ссылки.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Версия CMS.
        /// 
        /// Необязательный элемент.
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Список предложений магазина. Каждое предложение описывается в отдельном элементе offer.
        /// Здесь не приводится список всех элементов, входящих в offer,
        /// так как он зависит от типа предложения.
        /// Для большинства категорий товаров подходят следующие типы описаний:
        ///     Упрощенный тип
        ///     Произвольный тип
        /// Для некоторых категорий товаров нужно использовать собственные типы описаний:
        ///     Лекарства
        ///     Книги
        ///     Аудиокниги
        ///     Музыкальная и видеопродукция
        ///     Билеты на мероприятия
        ///     Туры
        /// 
        /// Обязательный элемент.
        /// </summary>
        public virtual ICollection<YmlOffer> Offers { get; set; }

        /// <summary>
        /// Подарки, которые не размещаются на Маркете (для акции "Подарок на выбор").
        /// </summary>
        public virtual ICollection<YmlGift> Gifts { get; set; }

        /// <summary>
        /// Промоакции.
        /// </summary>
        public virtual ICollection<YmlPromo> Promos { get; set; }


        public YmlShop()
        {
            Categories = new HashSet<YmlCategory>();
            Currencies = new HashSet<YmlCurrency>();
            DeliveryOptions = new HashSet<YmlShopDeliveryOption>();

            Offers = new HashSet<YmlOffer>();
            Gifts = new HashSet<YmlGift>();
            Promos = new HashSet<YmlPromo>();
        }
    }
}
