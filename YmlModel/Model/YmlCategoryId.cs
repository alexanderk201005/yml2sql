﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YmlModel.Model
{
    public enum YmlCategoryIdType
    {

        Yandex,

        Torg,

        Own,
    }

    public class YmlCategoryId
    {
        [Key]
        [ForeignKey("Offer")]
        public long Key { get; set; }

        public YmlCategoryIdType Type { get; set; }

        /// <summary>
        /// Название категории товара.
        /// 
        /// Обязательный параметр
        /// </summary>
        public string Value { get; set; }

        public virtual YmlOffer Offer { get; set; }

        public YmlCategoryId()
        {
            Type = YmlCategoryIdType.Own;
        }
    }
}
