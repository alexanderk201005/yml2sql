﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YmlModel.Model
{
    /// <summary>
    /// Цена на время акции.
    /// </summary>
    public class YmlDiscountPrice
    {
        [Key]
        [ForeignKey("Product")]
        public long Key { get; set; }

        /// <summary>
        /// Валюта.
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Новая цена.
        /// </summary>
        public string Value { get; set; }

        public virtual YmlProduct Product { get; set; }
    }
}
