﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YmlModel.Model
{
    /// <summary>
    /// Описание акции.
    /// </summary>
    public class YmlPromo
    {
        [Key]
        public long Key { get; set; }

        /// <summary>
        /// Идентификатор акции.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Тип акции.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Краткое описание акции. 
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Ссылка на описание акции на сайте магазина.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Текст промокода. Максимальная длина — 20 символов.
        /// </summary>
        public string PromoCode { get; set; }

        /// <summary>
        /// Размер скидки в процентах от стоимости или в валюте.
        /// </summary>
        public virtual YmlDiscount Discount { get; set; }

        /// <summary>
        /// Дата и время начала акции. Допустимые форматы: YYYY-MM-DD hh:mm:ss или YYYY-MM-DD.
        /// </summary>
        public string StartDate { get; set; }

        /// <summary>
        /// Дата и время завершения акции. Допустимые форматы: YYYY-MM-DD hh:mm:ss или YYYY-MM-DD.
        /// </summary>
        public string EndDate { get; set; }

        /// <summary>
        /// Информация о товарах, участвующих в акции.
        /// </summary>
        public virtual YmlPurchase Purchase { get; set; }

        /// <summary>
        /// Подарки, участвующие в акции. Максимальное число подарков на выбор — 12.
        /// </summary>
        public virtual ICollection<YmlPromoGift> PromoGifts { get; set; }

        public long YmlShopKey { get; set; }

        public YmlPromo()
        {
            PromoGifts = new HashSet<YmlPromoGift>();
        }
    }
}
