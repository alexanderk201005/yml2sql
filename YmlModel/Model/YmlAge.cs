﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YmlModel.Model
{
    /// <summary>
    /// Возрастная категория товара.
    /// </summary>
    public class YmlAge
    {
        [Key]
        [ForeignKey("Offer")]
        public long Key { get; set; }

        /// <summary>
        /// Годы(year) или месяцы(month)
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// Значение параметра.
        /// </summary>
        public string Value { get; set; }

        public virtual YmlOffer Offer { get; set; }
    }
}
