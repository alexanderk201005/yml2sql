﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YmlModel.Model
{
    /// <summary>
    /// Пользовательские параметры для PriceLabs
    /// 
    /// Пример:
    ///     Параметр для фильтрации по остаткам на складе:
    ///     <pricelabs_param namе="Остаток на складе">15</pricelabs_param >
    /// </summary>
    public class YmlPriceLabsParam
    {
        [Key]
        public long Key { get; set; }

        /// <summary>
        /// Имя параметра.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Значение параметра
        /// </summary>
        public string Value { get; set; }

        public long YmlOfferKey { get; set; }
    }
}
