﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YmlModel.Model
{
    /// <summary>
    /// Цена, по которой данный товар можно приобрести.
    /// </summary>
    public class YmlPrice
    {
        [Key]
        [ForeignKey("Offer")]
        public long Key { get; set; }

        /// <summary>
        /// Начальная цена «от» (true).
        /// </summary>
        public bool? From { get; set; }

        /// <summary>
        /// Цена.
        /// </summary>
        public string Value { get; set; }

        public virtual YmlOffer Offer { get; set; }

    }
}
