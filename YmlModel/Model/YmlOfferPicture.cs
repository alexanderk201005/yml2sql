﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YmlModel.Model
{
    /// <summary>
    /// Картинка товара.
    /// </summary>
    public class YmlOfferPicture
    {
        [Key]
        public long Key { get; set; }

        /// <summary>
        /// URL-ссылка на картинку товара.
        /// </summary>
        public string Url { get; set; }

        public long YmlOfferKey { get; set; }
    }
}
