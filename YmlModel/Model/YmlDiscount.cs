﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YmlModel.Model
{
    /// <summary>
    /// Размер скидки в процентах от стоимости или в валюте. Тип скидки указывается в атрибутах:
    ///     unit="percent" — скидка в процентах, доступные значения: от 5 до 95 процентов;
    ///     unit="currency" currency="RUR" — скидка в валюте, доступные значения: кратные 100.
    /// </summary>
    public class YmlDiscount
    {
        [Key]
        [ForeignKey("Promo")]
        public long Key { get; set; }

        /// <summary>
        /// Тип скидки(в процентах или валюте).
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// Валюта.
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Размер скидки.
        /// </summary>
        public string Value { get; set; }

        public virtual YmlPromo Promo { get; set; }
    }
}
