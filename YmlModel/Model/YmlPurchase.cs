﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YmlModel.Model
{
    /// <summary>
    /// Информация о товарах, участвующих в акции.
    /// </summary>
    public class YmlPurchase
    {
        [Key]
        [ForeignKey("Promo")]
        public long Key { get; set; }

        /// <summary>
        /// Количество товаров, которое нужно приобрести, чтобы получить подарок.
        /// </summary>
        public string RequiredQuantity { get; set; }

        /// <summary>
        /// Количество товаров, которые покупатель получит в подарок.
        /// </summary>
        public string FreeQuantity { get; set; }

        /// <summary>
        /// Товары и/или категории, на которые действует акция
        /// </summary>
        public virtual ICollection<YmlProduct> Products { get; set; }

        public virtual YmlPromo Promo { get; set; }

        public YmlPurchase()
        {
            Products = new HashSet<YmlProduct>();
        }
    }
}
