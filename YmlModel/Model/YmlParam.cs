﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YmlModel.Model
{
    /// <summary>
    /// Характеристика товара.
    /// </summary>
    public class YmlParam
    {
        [Key]
        public long Key { get; set; }

        /// <summary>
        /// Название параметра.
        /// 
        /// Обязательный парметер.
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Единицы измерения (для числовых параметров, опционально).
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// Значение параметра
        /// </summary>
        public string Value { get; set; }

        public long YmlOfferKey { get; set; }
    }
}
